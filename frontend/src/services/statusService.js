import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import { setStatusList, setError } from '../store/status/actions';

export const getStatusList = async (dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/statuses`);
    dispatch(setStatusList(response.data));
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    dispatch(endLoading());
  }
};

const statusService = {
  getStatusList,
};

export default statusService;
