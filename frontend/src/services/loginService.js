import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import { login, logout } from '../store/login/actions';
import { setUser } from './localStorageService';

const loginService = async (values, dispatch) => {
  dispatch(startLoading());
  try {
    API.resetErrorInterceptor();
    const response = await API.post('/authorize', values);
    const { jwtToken, role, id } = response.data;
    setUser(role, jwtToken, id);
    dispatch(login(role, jwtToken, id));
  } catch (ex) {
    dispatch(logout());
    throw ex;
  } finally {
    API.setErrorInterceptors();
    dispatch(endLoading());
  }
};

export default loginService;
