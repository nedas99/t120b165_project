import API from './api/api';

const getUserInfo = async (id) => {
  const userInfo = await API.get(`/users/${id}`);
  return userInfo.data;
};

export default getUserInfo;
