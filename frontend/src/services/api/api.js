import axios from 'axios';
import refreshToken from '../refreshTokenService';
import { getAccessToken } from '../localStorageService';

const BASE_URL = 'http://localhost:5000/api';

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  withCredentials: true,
});

const handleRequest = (req) => {
  // add jwt logic later
  const jwt = getAccessToken();
  if (jwt) {
    req.headers.Authorization = `Bearer ${jwt}`;
  }
  return req;
};
axiosInstance.interceptors.request.use((req) => handleRequest(req));

const handleResponseError = async (err) => {
  const config = err.config;
  if (
    (err.response.status === 401 || err.response.status === 403) &&
    !config._retry
  ) {
    await refreshToken();
    config._retry = true;
    console.log('trying again');
    axiosInstance(config);
  }
  return err;
  //return Promise.reject(err);
};

const API = {
  get(path, body) {
    return axiosInstance.get(path, body);
  },
  post(path, body) {
    return axiosInstance.post(path, body);
  },
  put(path, body) {
    return axiosInstance.put(path, body);
  },
  delete(path) {
    return axiosInstance.delete(path);
  },
  setErrorInterceptors() {
    axiosInstance.interceptors.response.use(
      (res) => res,
      (err) => handleResponseError(err),
    );
  },
  resetErrorInterceptor() {
    const index = axiosInstance.interceptors.response.handlers.length - 1;
    axiosInstance.interceptors.response.eject(index);
  },
};

export default API;
