import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import {
  setRegistrationAction,
  setDeliveryAction,
  setError,
  resetActions,
} from '../store/actions/actions';

export const getActions = async (userId, itemId, dispatch) => {
  dispatch(startLoading());
  dispatch(resetActions());
  try {
    API.setErrorInterceptors();
    const response = await API.get(`/users/${userId}/items/${itemId}/actions`);
    const [registration, delivery] = response.data;
    dispatch(setRegistrationAction(registration));
    dispatch(setDeliveryAction(delivery));
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    API.resetErrorInterceptor();
    dispatch(endLoading());
  }
};

const actionsService = {
  getActions,
};

export default actionsService;
