export const setUser = (role, token, id) => {
  localStorage.setItem(
    'user',
    JSON.stringify({
      role,
      token,
      id,
    }),
  );
};

export const resetUser = () => {
  localStorage.removeItem('user');
};

export const getUser = () => {
  const userInfo = localStorage.getItem('user');
  return userInfo
    ? {
        user: JSON.parse(userInfo),
      }
    : {};
};

export const getAccessToken = () => {
  const user = localStorage.getItem('user');
  if (!user) return null;

  return JSON.parse(user).token || null;
};

const localStorageService = {
  getAccessToken,
  getUser,
  resetUser,
  setUser,
};

export default localStorageService;
