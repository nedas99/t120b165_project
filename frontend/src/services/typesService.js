import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import { setTypesList, setError } from '../store/types/actions';

export const getTypesList = async (dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/types`);
    dispatch(setTypesList(response.data));
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    dispatch(endLoading());
  }
};

const typesService = {
  getTypesList,
};

export default typesService;
