import API from './api/api';
import { login } from '../store/login/actions';
import { getDispatch } from './dispatchService';
import { setUser } from './localStorageService';

export const refreshToken = async () => {
  try {
    const response = await API.post('/authorize/refreshToken');
    const { role, jwtToken, id } = response.data;
    setUser(role, jwtToken, id);
    const dispatch = getDispatch();
    dispatch(login(role, jwtToken, id));
  } catch (ex) {
    console.log('error with getting the refresh token');
  }
};

export default refreshToken;
