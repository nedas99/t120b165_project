import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import { setItemList, setError } from '../store/items/actions';

export const getItemList = async (userId, page, dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/users/${userId}/items?page=${page}`);
    dispatch(
      setItemList(
        response.data.previousPage,
        response.data.nextPage,
        response.data.items,
      ),
    );
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    dispatch(endLoading());
  }
};

export const getRegisteredItemList = async (page, dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/items/registered?page=${page}`);
    dispatch(
      setItemList(
        response.data.previousPage,
        response.data.nextPage,
        response.data.items,
      ),
    );
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    dispatch(endLoading());
  }
};

export const getAllItems = async (page, dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/items?page=${page}`);
    dispatch(
      setItemList(
        response.data.previousPage,
        response.data.nextPage,
        response.data.items,
      ),
    );
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    dispatch(endLoading());
  }
};

const itemService = {
  getItemList,
  getRegisteredItemList,
  getAllItems,
};

export default itemService;
