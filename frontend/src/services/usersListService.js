import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import {
  setUserList,
  setError,
  setSingleUser,
} from '../store/usersList/actions';
import {
  setCourier,
  setSender,
  setError as setItemUsersError,
} from '../store/itemUsers/actions';

export const getItemUsers = async (courierId, senderId, dispatch) => {
  dispatch(startLoading());
  try {
    const courierResponse = await API.get(`/users/${courierId}`);
    const senderResponse = await API.get(`/users/${senderId}`);
    dispatch(setCourier(courierResponse.data));
    dispatch(setSender(senderResponse.data));
  } catch (ex) {
    dispatch(setItemUsersError(ex));
  } finally {
    dispatch(endLoading());
  }
};

export const getUserList = async (page, dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/users?page=${page}`);
    dispatch(
      setUserList(
        response.data.previousPage,
        response.data.nextPage,
        response.data.users,
      ),
    );
  } catch (ex) {
    dispatch(setError('Error: ' + ex));
  } finally {
    dispatch(endLoading());
  }
};

export const getSingleUser = async (userId, dispatch) => {
  dispatch(startLoading());
  try {
    const response = await API.get(`/users/${userId}`);
    dispatch(setSingleUser(response.data));
  } catch (ex) {
    dispatch(setError('Error: ' + ex));
  } finally {
    dispatch(endLoading());
  }
};

const usersListService = {
  getUserList,
  getSingleUser,
  getItemUsers,
};

export default usersListService;
