import API from './api/api';
import { startLoading, endLoading } from '../store/loading/actions';
import { setLocations, setError } from '../store/locations/actions';
import { getDispatch } from './dispatchService';

export const getLocations = async () => {
  const dispatch = getDispatch();
  dispatch(startLoading());
  try {
    const response = await API.get(`/locations`);
    dispatch(setLocations(response.data));
  } catch (ex) {
    setError('Error: ' + ex);
  } finally {
    dispatch(endLoading());
  }
};

const locationService = {
  getLocations,
};

export default locationService;
