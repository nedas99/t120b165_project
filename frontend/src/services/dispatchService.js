let dispatch;

export const getDispatch = () => {
  return dispatch;
};

export const setDispatch = (d) => {
  dispatch = d;
};

const dispatchService = {
  getDispatch,
  setDispatch,
};

export default dispatchService;
