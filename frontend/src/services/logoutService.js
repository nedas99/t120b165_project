import { startLoading, endLoading } from '../store/loading/actions';
import { logout } from '../store/login/actions';
import { resetUser } from './localStorageService';
import API from './api/api';

const logoutService = async (dispatch) => {
  await dispatch(startLoading());
  try {
    await dispatch(logout());
    API.resetErrorInterceptor();
    await API.post('/authorize/revokeToken', {});
  } catch (ex) {
    console.log('Error while logging out');
  } finally {
    resetUser();
    API.setErrorInterceptors();
    dispatch(endLoading());
  }
};

export default logoutService;
