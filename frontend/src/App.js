import React, { useState } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setDispatch } from './services/dispatchService';
import refreshToken from './services/refreshTokenService';
import Navbar from './components/Navbar';
import LoginPage from './pages/LoginPage';
import MainPage from './pages/MainPage';
import UsersListPage from './pages/UsersListPage';
import RegisterPage from './pages/RegisterPage';
import LocationsPage from './pages/LocationsPage';
import AddLocationPage from './pages/AddLocationPage';
import ItemListPage from './pages/ItemListPage';
import AddItemPage from './pages/AddItemPage';
import ItemPage from './pages/ItemPage';
import RegisteredItemListPage from './pages/RegisteredItemListPage';
import exportObj from './pages/RegisteredItemPage';
import CourierItemListPage from './pages/CourierItemListPage';
import CourierItemPage from './pages/CourierItemPage';
import StatusesPage from './pages/StatusesPage';
import TypesPage from './pages/TypesPage';
import AdminItemListPage from './pages/AdminItemListPage';
import AdminItemPage from './pages/AdminItemPage';
import Footer from './components/Footer';
import Sidebar from './components/Sidebar';

import deliveryIcon from './images/delivery.svg';
import itemIcon from './images/item.svg';
import locationIcon from './images/location.svg';
import statusIcon from './images/status.svg';
import typeIcon from './images/type.svg';
import userIcon from './images/user.svg';

const mainMenuNavbarTabs = [
  /*{
    role: 'All',
    link: '/main',
    name: 'Main menu',
    component: <MainPage />,
    isTab: true,
  },*/
  {
    role: 'Admin',
    link: '/users',
    name: 'Users',
    component: <UsersListPage role="Admin" />,
    isTab: true,
    icon: userIcon,
  },
  {
    role: 'Admin',
    link: '/locations',
    name: 'Locations',
    component: <LocationsPage role="Admin" />,
    isTab: true,
    icon: locationIcon,
  },
  {
    role: 'Admin',
    link: '/locations/add',
    name: 'Add location',
    component: <AddLocationPage role="Admin" />,
  },
  {
    role: 'Sender',
    link: '/items',
    name: 'Items',
    component: <ItemListPage role="Sender" />,
    isTab: true,
    icon: itemIcon,
  },
  {
    role: 'Sender',
    link: '/addItem',
    name: 'Add item',
    component: <AddItemPage role="Sender" />,
  },
  {
    role: 'Sender',
    link: '/items/:itemId',
    name: 'Specific item',
    component: <ItemPage role="Sender" />,
  },
  {
    role: 'Courier',
    link: '/registeredItems',
    name: 'Registered items',
    component: <RegisteredItemListPage role="Courier" />,
    isTab: true,
    icon: itemIcon,
  },
  {
    role: 'Courier',
    link: '/registeredItems/:itemId',
    name: 'Specific registered item',
    component: <exportObj.RegisteredItemPage role="Courier" />,
  },
  {
    role: 'Courier',
    link: '/courier/items',
    name: 'Assigned items',
    component: <CourierItemListPage role="Courier" />,
    isTab: true,
    icon: deliveryIcon,
  },
  {
    role: 'Courier',
    link: '/courier/items/:itemId',
    name: 'Specific assigned item',
    component: <CourierItemPage role="Courier" />,
  },
  {
    role: 'Admin',
    link: '/statuses',
    name: 'Statuses',
    component: <StatusesPage role="Admin" />,
    isTab: true,
    icon: statusIcon,
  },
  {
    role: 'Admin',
    link: '/types',
    name: 'Types',
    component: <TypesPage role="Admin" />,
    isTab: true,
    icon: typeIcon,
  },
  {
    role: 'Admin',
    link: '/admin/items',
    name: 'Items',
    component: <AdminItemListPage role="Admin" />,
    isTab: true,
    icon: itemIcon,
  },
  {
    role: 'Admin',
    link: '/admin/items/:itemId',
    name: 'Specific item',
    component: <AdminItemPage role="Admin" />,
  },
];

const App = () => {
  setDispatch(useDispatch());
  refreshToken();
  //API.setErrorInterceptors();

  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <div id="App">
      <Router>
        <Sidebar
          open={menuOpen}
          setOpen={setMenuOpen}
          tabs={mainMenuNavbarTabs}
        />
        <Navbar tabs={mainMenuNavbarTabs} setMenuOpen={setMenuOpen} />
        <br />
        <Route exact path="/">
          <LoginPage />
        </Route>
        <Route exact path="/register">
          <RegisterPage />
        </Route>
        <Route exact path="/main">
          <MainPage tabs={mainMenuNavbarTabs} />
        </Route>

        {mainMenuNavbarTabs.map((t, index) => (
          <Route key={index} exact path={t.link}>
            {t.component}
          </Route>
        ))}
        <br />
        <Footer />
      </Router>
    </div>
  );
};

export default App;
