import {
  SET_STATUS_LIST,
  RESET_STATUS_LIST,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

const INITIAL_STATE = {
  list: [],
  error: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_STATUS_LIST: {
      return {
        ...state,
        list: payload.statusList,
        error: null,
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case RESET_STATUS_LIST: {
      return {
        ...state,
        list: [],
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
