import {
  SET_STATUS_LIST,
  RESET_STATUS_LIST,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

export const setStatusList = (statusList) => ({
  type: SET_STATUS_LIST,
  payload: {
    statusList,
  },
});

export const resetStatusList = () => ({
  type: RESET_STATUS_LIST,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

const statusListActions = {
  setError,
  resetError,
  setStatusList,
  resetStatusList,
};

export default statusListActions;
