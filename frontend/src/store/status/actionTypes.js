export const SET_STATUS_LIST = 'SET_STATUS_LIST';
export const RESET_STATUS_LIST = 'RESET_STATUS_LIST';
export const SET_ERROR = 'SET_ERROR';
export const RESET_ERROR = 'RESET_ERROR';
