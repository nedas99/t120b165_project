import {
  SET_LOCATIONS,
  RESET_LOCATIONS,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

export const setLocations = (locations) => ({
  type: SET_LOCATIONS,
  payload: {
    locations,
  },
});

export const resetLocations = () => ({
  type: RESET_LOCATIONS,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

const locationsActions = {
  setLocations,
  resetLocations,
  setError,
  resetError,
};

export default locationsActions;
