import {
  SET_LOCATIONS,
  RESET_LOCATIONS,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

const INITIAL_STATE = {
  locations: [],
  error: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_LOCATIONS: {
      return {
        ...state,
        locations: payload.locations,
        error: null,
      };
    }
    case RESET_LOCATIONS: {
      return {
        ...state,
        locations: [],
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
