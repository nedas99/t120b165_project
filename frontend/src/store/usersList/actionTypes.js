export const SET_USER_LIST = 'SET_USER_LIST';
export const RESET_USER_LIST = 'RESET_USER_LIST';
export const SET_ERROR = 'SET_ERROR';
export const RESET_ERROR = 'RESET_ERROR';
export const SET_PAGE = 'SET_PAGE';
export const SET_SINGLE_USER = 'SET_SINGLE_USER';
