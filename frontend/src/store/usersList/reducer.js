import {
  SET_USER_LIST,
  RESET_USER_LIST,
  SET_ERROR,
  RESET_ERROR,
  SET_PAGE,
  SET_SINGLE_USER,
} from './actionTypes';

const INITIAL_STATE = {
  previous: false,
  next: false,
  users: [],
  error: null,
  page: 1,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_PAGE: {
      return {
        ...state,
        page: payload.page,
      };
    }
    case SET_USER_LIST: {
      return {
        ...state,
        previous: payload.previous,
        next: payload.next,
        users: payload.users,
        error: null,
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case RESET_USER_LIST: {
      return {
        ...state,
        previous: false,
        next: false,
        users: [],
        page: 1,
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    case SET_SINGLE_USER: {
      return {
        ...state,
        previous: false,
        next: false,
        users: [payload.user],
        page: 1,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
