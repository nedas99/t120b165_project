import {
  SET_USER_LIST,
  RESET_USER_LIST,
  SET_ERROR,
  RESET_ERROR,
  SET_PAGE,
  SET_SINGLE_USER,
} from './actionTypes';

export const setPage = (page) => ({
  type: SET_PAGE,
  payload: {
    page,
  },
});

export const setUserList = (previous, next, users) => ({
  type: SET_USER_LIST,
  payload: {
    previous,
    next,
    users,
  },
});

export const resetUserList = () => ({
  type: RESET_USER_LIST,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

export const setSingleUser = (user) => ({
  type: SET_SINGLE_USER,
  payload: {
    user,
  },
});

const usersListActions = {
  setError,
  resetError,
  setUserList,
  resetUserList,
  setPage,
  setSingleUser,
};

export default usersListActions;
