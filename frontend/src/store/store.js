import { createStore, combineReducers } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import { reducer as loginReducer } from './login';
import { reducer as loadingReducer } from './loading';
import { reducer as usersListReducer } from './usersList';
import { reducer as locationsReducer } from './locations';
import { reducer as itemReducer } from './items';
import { reducer as statusReducer } from './status';
import { reducer as typesReducer } from './types';
import { reducer as actionsReducer } from './actions';
import { reducer as itemUsersReducer } from './itemUsers';
import { getUser } from '../services/localStorageService';

const reducers = {
  user: loginReducer,
  loading: loadingReducer,
  usersList: usersListReducer,
  locations: locationsReducer,
  itemList: itemReducer,
  status: statusReducer,
  types: typesReducer,
  actions: actionsReducer,
  itemUsers: itemUsersReducer,
};

const rootReducer = combineReducers(reducers);

const persistedState = getUser();

const store = createStore(rootReducer, persistedState, devToolsEnhancer());

export default store;
