import {
  SET_TYPES_LIST,
  RESET_TYPES_LIST,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

export const setTypesList = (typesList) => ({
  type: SET_TYPES_LIST,
  payload: {
    typesList,
  },
});

export const resetTypesList = () => ({
  type: RESET_TYPES_LIST,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

const typesListActions = {
  setError,
  resetError,
  setTypesList,
  resetTypesList,
};

export default typesListActions;
