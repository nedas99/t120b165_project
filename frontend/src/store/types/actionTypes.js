export const SET_TYPES_LIST = 'SET_TYPES_LIST';
export const RESET_TYPES_LIST = 'RESET_TYPES_LIST';
export const SET_ERROR = 'SET_ERROR';
export const RESET_ERROR = 'RESET_ERROR';
