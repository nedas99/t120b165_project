import {
  SET_TYPES_LIST,
  RESET_TYPES_LIST,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

const INITIAL_STATE = {
  list: [],
  error: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_TYPES_LIST: {
      return {
        ...state,
        list: payload.typesList,
        error: null,
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case RESET_TYPES_LIST: {
      return {
        ...state,
        list: [],
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
