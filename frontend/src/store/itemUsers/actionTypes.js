export const SET_ERROR = 'SET_ERROR';
export const RESET_ERROR = 'RESET_ERROR';
export const SET_COURIER = 'SET_COURIER';
export const SET_SENDER = 'SET_SENDER';
export const RESET_COURIER = 'RESET_COURIER';
export const RESET_SENDER = 'RESET_SENDER';
