import {
  SET_ERROR,
  RESET_ERROR,
  SET_COURIER,
  RESET_COURIER,
  SET_SENDER,
  RESET_SENDER,
} from './actionTypes';

const INITIAL_STATE = {
  courier: null,
  sender: null,
  error: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_COURIER: {
      return {
        ...state,
        error: null,
        courier: payload.user,
      };
    }
    case RESET_COURIER: {
      return {
        ...state,
        courier: null,
      };
    }
    case SET_SENDER: {
      return {
        ...state,
        error: null,
        sender: payload.user,
      };
    }
    case RESET_SENDER: {
      return {
        ...state,
        sender: null,
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
