import {
  SET_ERROR,
  RESET_ERROR,
  SET_COURIER,
  RESET_COURIER,
  SET_SENDER,
  RESET_SENDER,
} from './actionTypes';

export const setCourier = (user) => ({
  type: SET_COURIER,
  payload: {
    user,
  },
});

export const resetCourier = () => ({
  type: RESET_COURIER,
  payload: {},
});

export const setSender = (user) => ({
  type: SET_SENDER,
  payload: {
    user,
  },
});

export const resetSender = () => ({
  type: RESET_SENDER,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

const itemUsersActions = {
  setError,
  resetError,
  setCourier,
  resetCourier,
  setSender,
  resetSender,
};

export default itemUsersActions;
