import { START_LOADING, END_LOADING } from './actionTypes';

export const startLoading = () => ({
  type: START_LOADING,
});

export const endLoading = () => ({
  type: END_LOADING,
});

const loadingActions = {
  startLoading,
  endLoading,
};

export default loadingActions;
