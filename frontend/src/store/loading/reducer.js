import { START_LOADING, END_LOADING } from './actionTypes';

const INITIAL_STATE = {
  loading: false,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type } = action;

  switch (type) {
    case START_LOADING: {
      return {
        ...state,
        loading: true,
      };
    }

    case END_LOADING: {
      return {
        ...state,
        loading: false,
      };
    }

    default: {
      return state;
    }
  }
};

export default reducer;
