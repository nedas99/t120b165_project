import { LOGIN, LOGOUT } from './actionTypes';

export const login = (role, token, id) => ({
  type: LOGIN,
  payload: {
    role,
    token,
    id,
  },
});

export const logout = () => ({
  type: LOGOUT,
  payload: {},
});

const loginActions = {
  login,
  logout,
};

export default loginActions;
