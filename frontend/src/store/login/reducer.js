import { LOGIN, LOGOUT } from './actionTypes';

const INITIAL_STATE = {
  role: null,
  token: null,
  id: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case LOGIN: {
      return {
        ...state,
        role: payload.role,
        token: payload.token,
        id: payload.id,
      };
    }

    case LOGOUT: {
      return {
        ...state,
        role: null,
        token: null,
        id: null,
      };
    }

    default: {
      return state;
    }
  }
};

export default reducer;
