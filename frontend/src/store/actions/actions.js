import {
  SET_REGISTRATION_ACTION,
  SET_DELIVERY_ACTION,
  RESET_ACTIONS,
  SET_ERROR,
  RESET_ERROR,
} from './actionTypes';

export const setRegistrationAction = (action) => ({
  type: SET_REGISTRATION_ACTION,
  payload: {
    action,
  },
});

export const setDeliveryAction = (action) => ({
  type: SET_DELIVERY_ACTION,
  payload: {
    action,
  },
});

export const resetActions = () => ({
  type: RESET_ACTIONS,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

const actionsActions = {
  setError,
  resetError,
  setRegistrationAction,
  setDeliveryAction,
  resetActions,
};

export default actionsActions;
