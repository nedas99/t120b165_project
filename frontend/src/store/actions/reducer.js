import {
  SET_REGISTRATION_ACTION,
  SET_DELIVERY_ACTION,
  SET_ERROR,
  RESET_ERROR,
  RESET_ACTIONS,
} from './actionTypes';

const INITIAL_STATE = {
  registration: null,
  delivery: null,
  error: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_REGISTRATION_ACTION: {
      return {
        ...state,
        registration: payload.action,
        error: null,
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case SET_DELIVERY_ACTION: {
      return {
        ...state,
        delivery: payload.action,
        error: null,
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    case RESET_ACTIONS: {
      return {
        ...state,
        registration: null,
        delivery: null,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
