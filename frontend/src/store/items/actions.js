import {
  SET_ITEM_LIST,
  RESET_ITEM_LIST,
  SET_ERROR,
  RESET_ERROR,
  SET_PAGE,
} from './actionTypes';

export const setPage = (page) => ({
  type: SET_PAGE,
  payload: {
    page,
  },
});

export const setItemList = (previous, next, items) => ({
  type: SET_ITEM_LIST,
  payload: {
    previous,
    next,
    items,
  },
});

export const resetItemList = () => ({
  type: RESET_ITEM_LIST,
  payload: {},
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error,
  },
});

export const resetError = () => ({
  type: RESET_ERROR,
  payload: {},
});

const itemListActions = {
  setError,
  resetError,
  setItemList,
  resetItemList,
  setPage,
};

export default itemListActions;
