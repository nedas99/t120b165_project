export const SET_ITEM_LIST = 'SET_ITEM_LIST';
export const RESET_ITEM_LIST = 'RESET_ITEM_LIST';
export const SET_ERROR = 'SET_ERROR';
export const RESET_ERROR = 'RESET_ERROR';
export const SET_PAGE = 'SET_PAGE';
