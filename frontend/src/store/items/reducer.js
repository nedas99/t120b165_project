import {
  SET_ITEM_LIST,
  RESET_ITEM_LIST,
  SET_ERROR,
  RESET_ERROR,
  SET_PAGE,
} from './actionTypes';

const INITIAL_STATE = {
  previous: false,
  next: false,
  items: [],
  error: null,
  page: 1,
};

const reducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_PAGE: {
      return {
        ...state,
        page: payload.page,
      };
    }
    case SET_ITEM_LIST: {
      return {
        ...state,
        previous: payload.previous,
        next: payload.next,
        items: payload.items,
        error: null,
      };
    }
    case SET_ERROR: {
      return {
        ...state,
        error: payload.error,
      };
    }
    case RESET_ITEM_LIST: {
      return {
        ...state,
        previous: false,
        next: false,
        items: [],
        page: 1,
      };
    }
    case RESET_ERROR: {
      return {
        ...state,
        error: null,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
