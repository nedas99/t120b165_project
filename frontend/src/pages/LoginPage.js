import React from 'react';
import Login from '../components/Login';

const LoginPage = () => {
  return (
    <div className="flex">
      <div className="div-centered login-border">
        <h1 className="margin-bottom">Shipment Application</h1>
        <Login />
      </div>
    </div>
  );
};

export default LoginPage;
