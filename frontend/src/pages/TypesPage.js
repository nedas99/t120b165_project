import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { getTypesList } from '../services/typesService';
import ReactModal from 'react-modal';
import API from '../services/api/api';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '50px',
  },
};

const DeleteModal = ({ isOpen, setIsOpen, fetchTypes, typeId }) => {
  if (!typeId) {
    return null;
  }

  const handleDelete = async (id) => {
    const url = `/types/${id}`;
    try {
      await API.delete(url);
      fetchTypes();
      setIsOpen(false);
    } catch (ex) {
      console.log(ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isOpen}>
      <div>
        <h1>Are you sure you want to delete this type?</h1>
        <button className="button" onClick={() => setIsOpen(false)}>
          No
        </button>
        <button className="button" onClick={() => handleDelete(typeId)}>
          Yes
        </button>
      </div>
    </ReactModal>
  );
};

const EditModal = ({
  isOpen,
  setIsOpen,
  fetchTypes,
  typeId,
  name,
  setName,
}) => {
  const [error, setError] = useState('');

  if (!typeId) {
    return null;
  }

  const handleSubmit = async () => {
    if (!name || !name.trim().length) {
      setError('Empty field');
      return;
    }
    const dto = { name };
    const url = `/types/${typeId}`;
    try {
      await API.put(url, dto);
      fetchTypes();
      setName('');
      setIsOpen(false);
    } catch (ex) {
      setError('Error while editing type' + ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isOpen}>
      <button className="button" onClick={() => setIsOpen(false)}>
        Close
      </button>
      <p className="margin">Enter type value:</p>
      <input value={name} onChange={(e) => setName(e.target.value)} />
      {error && <div style={{ color: 'red' }}>{error}</div>}
      <button className="margin button" onClick={() => handleSubmit()}>
        Edit type
      </button>
    </ReactModal>
  );
};

const CreateModal = ({ isOpen, setIsOpen, fetchTypes }) => {
  const [name, setName] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = async () => {
    if (!name || !name.trim().length) {
      setError('Empty field');
      return;
    }
    const dto = { name };
    const url = '/types';
    try {
      await API.post(url, dto);
      fetchTypes();
      setName('');
      setIsOpen(false);
    } catch (ex) {
      setError('Error while creating type' + ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isOpen}>
      <button className="button" onClick={() => setIsOpen(false)}>
        Close
      </button>
      <p className="margin">Enter type value:</p>
      <input value={name} onChange={(e) => setName(e.target.value)} />
      {error && <div style={{ color: 'red' }}>{error}</div>}
      <button className="margin button" onClick={() => handleSubmit()}>
        Create new
      </button>
    </ReactModal>
  );
};

const TableBody = ({
  types,
  setTypeId,
  handleOpenEditModal,
  setDeleteOpen,
}) => {
  if (!types) {
    return null;
  }

  const getButtons = (id) => {
    return (
      <>
        <li>
          <button
            onClick={() => {
              setTypeId(id);
              handleOpenEditModal();
            }}
          >
            <i class="fas fa-pencil-alt"></i>
          </button>
        </li>
        <li>
          <button
            onClick={() => {
              setTypeId(id);
              setDeleteOpen(true);
            }}
          >
            <i class="fas fa-ban"></i>
          </button>
        </li>
      </>
    );
  };

  return (
    <tbody>
      {types.map((s) => (
        <tr key={s.id}>
          <td>{s.id}</td>
          <td>{s.name}</td>
          <td>{getButtons(s.id)}</td>
        </tr>
      ))}
    </tbody>
  );
};

const Table = ({ setTypeId, handleOpenEditModal, list, setDeleteOpen }) => {
  const error = useSelector((state) => state.types.error);

  if (error) {
    return <div style={{ color: 'red' }}>{error}</div>;
  }

  if (!list || list.length === 0) {
    return <div className="flex">There are no types</div>;
  }

  return (
    <div className="flex">
      <div className="lower-width">
        <table className="table table-dark table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>Type</th>
              <th>Actions</th>
            </tr>
          </thead>
          <TableBody
            types={list}
            setTypeId={setTypeId}
            handleOpenEditModal={handleOpenEditModal}
            setDeleteOpen={setDeleteOpen}
          />
        </table>
      </div>
    </div>
  );
};

const TypesPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  if (userRole !== role) {
    history.push('/main');
  }

  const dispatch = useDispatch();
  const fetchTypes = () => {
    getTypesList(dispatch);
  };

  const [createOpen, setCreateOpen] = useState(false);
  const [deleteOpen, setDeleteOpen] = useState(false);

  const [editOpen, setEditOpen] = useState(false);
  const [typeId, setTypeId] = useState(0);
  const [name, setName] = useState('');

  const list = useSelector((state) => state.types.list);

  useEffect(() => {
    fetchTypes();
  }, []);

  const handleOpenEditModal = () => {
    if (typeId) {
      const type = list.find((t) => t.id === typeId);
      type && setName(type.name);
      setEditOpen(true);
    }
  };

  useEffect(() => {
    if (typeId) {
      const type = list.find((t) => t.id === typeId);
      type && setName(type.name);
    }
  }, [typeId]);

  return (
    <>
      <div className="flex">
        <h1>Types</h1>
      </div>
      <div className="flex">
        <button className="button" onClick={() => setCreateOpen(true)}>
          Create new
        </button>
      </div>
      <div>
        <Table
          setTypeId={setTypeId}
          list={list}
          handleOpenEditModal={handleOpenEditModal}
          setDeleteOpen={setDeleteOpen}
        />
      </div>
      <CreateModal
        isOpen={createOpen}
        setIsOpen={setCreateOpen}
        fetchTypes={fetchTypes}
      />
      <EditModal
        isOpen={editOpen}
        setIsOpen={setEditOpen}
        fetchTypes={fetchTypes}
        typeId={typeId}
        name={name}
        setName={setName}
      />
      <DeleteModal
        isOpen={deleteOpen}
        setIsOpen={setDeleteOpen}
        fetchTypes={fetchTypes}
        typeId={typeId}
      />
    </>
  );
};

export default TypesPage;
