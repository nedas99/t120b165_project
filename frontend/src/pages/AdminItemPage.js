import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { getAllItems } from '../services/itemService';
import { getStatusList } from '../services/statusService';
import { getActions } from '../services/actionsService';
import { getItemUsers } from '../services/usersListService';
import { getTypesList } from '../services/typesService';
import { ItemData } from './RegisteredItemPage';
import Loader from 'react-loader-spinner';
import ReactModal from 'react-modal';
import API from '../services/api/api';
import ItemForm from '../components/ItemForm';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '50px',
  },
};

const EditItemModal = ({
  isModalOpen,
  setModalOpen,
  item,
  userId,
  history,
  fetchItems,
}) => {
  const [error, setError] = useState('');

  const getInitialValues = (item) => {
    const values = {
      name: item.name,
      width: item.width,
      length: item.length,
      height: item.height,
      type: item.typeId,
    };
    return values;
  };

  const onSubmit = async (values) => {
    const dto = {
      ...values,
      status: item.statusId,
    };

    try {
      await API.put(`/users/${userId}/items/${item.id}`, dto);
      fetchItems();
      history.push('/admin/items');
    } catch (err) {
      setError(err.response);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isModalOpen}>
      <div className="flex">
        <button className="button" onClick={() => setModalOpen(false)}>
          Close
        </button>
      </div>
      <div className="flex">
        <div className="div-centered login-border">
          <h1>Edit Item</h1>
          <ItemForm
            initialValues={getInitialValues(item)}
            onSubmit={onSubmit}
            error={error}
            buttonText="Edit Item"
          />
        </div>
      </div>
    </ReactModal>
  );
};

const UserTab = ({ title, user }) => {
  if (!user) {
    return null;
  }

  return (
    <table className="table table-dark table-hover">
      <thead>
        <tr>
          <th colSpan="2">{title}</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Email</td>
          <td>{user.email}</td>
        </tr>
        <tr>
          <td>Phone number</td>
          <td>{user.phoneNumber}</td>
        </tr>
      </tbody>
    </table>
  );
};

const AdminItemPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);
  const dispatch = useDispatch();

  if (userRole !== role) {
    history.push('/main');
  }

  const { itemId } = useParams();

  const item = useSelector((state) => state.itemList.items).find(
    (i) => i.id === parseInt(itemId),
  );

  if (!item) {
    history.push('/items');
  }

  const loading = useSelector((state) => state.loading.loading);
  const registration = useSelector((state) => state.actions.registration);
  const delivery = useSelector((state) => state.actions.delivery);

  const sender = useSelector((state) => state.itemUsers.sender);
  const courier = useSelector((state) => state.itemUsers.courier);

  const [editModalOpen, setEditModalOpen] = useState(false);

  const fetchItems = () => {
    const callItemsApi = async () => {
      await getAllItems(1, dispatch);
    };
    callItemsApi();
  };

  const fetchActions = () => {
    getActions(item.senderId, itemId, dispatch);
  };

  const fetchUsers = () => {
    getItemUsers(item.courierId, item.senderId, dispatch);
  };

  const fetchStatuses = () => {
    getStatusList(dispatch);
  };

  const fetchTypes = () => {
    getTypesList(dispatch);
  };

  useEffect(() => {
    fetchActions();
    fetchUsers();
    fetchStatuses();
    fetchTypes();
  }, []);

  return (
    <div className="flex">
      <div className="div-centered">
        <ItemData
          item={item}
          registration={registration}
          delivery={delivery}
          loading={loading}
        />
        <UserTab title="Sender Information" user={sender} />
        <UserTab title="Courier Information" user={courier} />
        <button className="button" onClick={() => setEditModalOpen(true)}>
          Edit
        </button>
        <Loader visible={loading} type="TailSpin" color="Blue" />
      </div>
      <EditItemModal
        isModalOpen={editModalOpen}
        setModalOpen={setEditModalOpen}
        item={item}
        userId={item.senderId}
        history={history}
        fetchItems={fetchItems}
      />
    </div>
  );
};

export default AdminItemPage;
