import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import LocationPicker from '../components/LocationPicker';
import ReactModal from 'react-modal';
import API from '../services/api/api';
import { getLocations } from '../services/locationService';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

ReactModal.setAppElement('#modal');

const AddLocationPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  if (userRole !== role) {
    history.push('/main');
  }

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [location, setLocation] = useState(null);
  const [description, setDescription] = useState('');

  const handlePostLocation = async () => {
    try {
      API.setErrorInterceptors();
      const body = {
        latitude: location.latitude,
        longitude: location.longitude,
        description,
      };
      await API.post('/locations', body);
      await getLocations();
      history.push('/locations');
    } catch (ex) {
      console.log(ex.message);
    } finally {
      API.resetErrorInterceptor();
    }
  };

  return (
    <>
      <div className="flex">
        <h1>Add location</h1>
      </div>
      <div className="flex margin-bottom">Pick a location and click "Next"</div>
      <div className="flex">
        <LocationPicker
          location={location}
          setLocation={setLocation}
          isModalOpen={isModalOpen}
        />
      </div>
      {location && (
        <div className="flex">
          <button
            onClick={() => {
              setIsModalOpen(true);
            }}
            className="button"
          >
            Next
          </button>
        </div>
      )}
      <ReactModal style={customStyles} isOpen={isModalOpen}>
        <div>
          <h1>Add a description to selected location, or go back</h1>
          <div className="flex">
            <input
              type="text"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          <button
            className="button"
            onClick={() => {
              handlePostLocation();
            }}
          >
            Create location
          </button>
          <button className="button" onClick={() => setIsModalOpen(false)}>
            Go back
          </button>
        </div>
      </ReactModal>
    </>
  );
};

export default AddLocationPage;
