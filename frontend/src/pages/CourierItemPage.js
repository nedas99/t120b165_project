import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { getActions } from '../services/actionsService';
import Loader from 'react-loader-spinner';
import { ItemData } from './RegisteredItemPage';
import { getSingleUser } from '../services/usersListService';
import API from '../services/api/api';
import { getItemList } from '../services/itemService';
import ReactModal from 'react-modal';
import { getStatusList } from '../services/statusService';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '50px',
  },
};

const SelectStatusModal = ({
  selectedStatus,
  setSelectedStatus,
  statusModalOpen,
  setStatusModalOpen,
  statusList,
  item,
  action,
  history,
  fetchItems,
}) => {
  const getItemDto = (item, statusId) => {
    const dto = {
      name: item.name,
      width: item.width,
      length: item.length,
      height: item.height,
      type: item.typeId,
      status: statusId,
    };
    return dto;
  };

  const getActionDto = (action) => {
    const dto = {
      location: action.locationId,
      endTime: new Date().toISOString(),
    };
    return dto;
  };

  const handleUpdatedStatus = async () => {
    const itemDto = getItemDto(item, selectedStatus);
    const itemUrl = `/users/${item.senderId}/items/${item.id}`;

    try {
      await API.put(itemUrl, itemDto);
      fetchItems();
      history.push('/courier/items');
    } catch (ex) {
      console.log(ex.message);
    }
  };

  const handleCompleteDelivery = async () => {
    const itemDto = getItemDto(item, 6);
    const itemUrl = `/users/${item.senderId}/items/${item.id}`;

    const actionUrl = `/users/${item.senderId}/items/${item.id}/actions/${action.id}`;
    const actionDto = getActionDto(action);

    try {
      await API.put(actionUrl, actionDto);
      await API.put(itemUrl, itemDto);
      fetchItems();
      history.push('/courier/items');
    } catch (ex) {
      console.log(ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={statusModalOpen}>
      <div className="flex">
        <button className="button" onClick={() => setStatusModalOpen(false)}>
          Close
        </button>
      </div>
      <div className="flex">
        <h1>Select new status</h1>
      </div>
      <div className="flex">
        <select
          className="field"
          value={selectedStatus}
          onChange={(e) => setSelectedStatus(e.target.value)}
        >
          <option value={0}>--------</option>
          {statusList.map((s) => (
            <option key={s.id} value={s.id}>
              {s.name}
            </option>
          ))}
        </select>
      </div>
      {selectedStatus > 0 && (
        <div className="flex">
          <button className="button" onClick={() => handleUpdatedStatus()}>
            Change status
          </button>
        </div>
      )}
      <div className="flex">
        <button className="button" onClick={() => handleCompleteDelivery()}>
          Set delivered
        </button>
      </div>
    </ReactModal>
  );
};

const OnRouteButton = ({ item, action, history, fetchItems }) => {
  const getItemDto = (item) => {
    const dto = {
      name: item.name,
      width: item.width,
      length: item.length,
      height: item.height,
      type: item.typeId,
      status: 3,
    };
    return dto;
  };

  const getActionDto = (action) => {
    const dto = {
      location: action.locationId,
      startTime: new Date().toISOString(),
    };
    return dto;
  };

  return (
    <button
      className="button"
      onClick={async () => {
        /*console.log(action);
        console.log(item);*/
        const itemUrl = `/users/${item.senderId}/items/${item.id}`;
        const actionUrl = `/users/${item.senderId}/items/${item.id}/actions/${action.id}`;

        try {
          await API.put(actionUrl, getActionDto(action));
          await API.put(itemUrl, getItemDto(item));
          fetchItems();
          history.push('/courier/items');
        } catch (ex) {
          console.log(ex.message);
        }
      }}
    >
      Set delivery on route
    </button>
  );
};

const UserTab = ({ user }) => {
  return (
    <table className="table table-dark table-hover">
      <thead>
        <tr>
          <th colSpan="2">Sender information</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Email</td>
          <td>{user.email}</td>
        </tr>
        <tr>
          <td>Phone number</td>
          <td>{user.phoneNumber}</td>
        </tr>
      </tbody>
    </table>
  );
};

const CourierItemPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);
  const dispatch = useDispatch();

  if (userRole !== role) {
    history.push('/main');
  }

  const { itemId } = useParams();
  const registration = useSelector((state) => state.actions.registration);
  const delivery = useSelector((state) => state.actions.delivery);
  const loading = useSelector((state) => state.loading.loading);
  const userId = useSelector((state) => state.user.id);
  const userList = useSelector((state) => state.usersList.users);
  const statusList = useSelector((state) => state.status.list);

  const [statusModalOpen, setStatusModalOpen] = useState(false);
  const [selectedStatus, setSelectedStatus] = useState(0);

  const itemSender = userList ? userList[0] : null;

  const item = useSelector((state) => state.itemList.items).find(
    (i) => i.id === parseInt(itemId),
  );

  if (!item) {
    history.push('/items');
  }

  const fetchItems = () => {
    const callItemsApi = async () => {
      await getItemList(userId, 1, dispatch);
    };
    callItemsApi();
  };

  const fetchActions = () => {
    getActions(item.senderId, itemId, dispatch);
  };

  const fetchUser = () => {
    getSingleUser(item.senderId, dispatch);
  };

  const fetchStatuses = () => {
    getStatusList(dispatch);
  };

  useEffect(() => {
    fetchActions();
    fetchUser();
    fetchStatuses();
  }, []);

  //return <ItemData item={item} registration={registration} delivery={delivery} loading={loading} />;
  return (
    <div className="flex">
      <div className="div-centered">
        <ItemData
          item={item}
          registration={registration}
          delivery={delivery}
          loading={loading}
        />
        {itemSender && <UserTab user={itemSender} />}
        {item.statusId === 2 && delivery && (
          <OnRouteButton
            action={delivery}
            item={item}
            history={history}
            fetchItems={fetchItems}
          />
        )}
        {item.statusId > 2 && item.status !== 'Delivered' && statusList && (
          <button className="button" onClick={() => setStatusModalOpen(true)}>
            Change status
          </button>
        )}
        <Loader visible={loading} type="TailSpin" color="Blue" />
      </div>
      {statusList && (
        <SelectStatusModal
          selectedStatus={selectedStatus}
          setSelectedStatus={setSelectedStatus}
          statusModalOpen={statusModalOpen}
          setStatusModalOpen={setStatusModalOpen}
          statusList={statusList}
          item={item}
          action={delivery}
          history={history}
          fetchItems={fetchItems}
        />
      )}
    </div>
  );
};

export default CourierItemPage;
