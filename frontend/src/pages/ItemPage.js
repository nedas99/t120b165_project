import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { getActions } from '../services/actionsService';
import ItemData from '../components/ItemData';
import Loader from 'react-loader-spinner';
import { getLocations } from '../services/locationService';
import API from '../services/api/api';
import SelectLocationModal from '../components/SelectLocationModal';
import { getSingleUser } from '../services/usersListService';
import { getTypesList } from '../services/typesService';
import ReactModal from 'react-modal';
import ItemForm from '../components/ItemForm';
import { getItemList } from '../services/itemService';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '50px',
  },
};

const EditItemModal = ({
  isModalOpen,
  setModalOpen,
  item,
  userId,
  history,
  fetchItems,
}) => {
  const [error, setError] = useState('');

  const getInitialValues = (item) => {
    const values = {
      name: item.name,
      width: item.width,
      length: item.length,
      height: item.height,
      type: item.typeId,
    };
    return values;
  };

  const onSubmit = async (values) => {
    const dto = {
      ...values,
      status: item.statusId,
    };

    try {
      await API.put(`/users/${userId}/items/${item.id}`, dto);
      fetchItems();
      history.push('/items');
    } catch (err) {
      setError(err.response);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isModalOpen}>
      <div className="flex">
        <button className="button" onClick={() => setModalOpen(false)}>
          Close
        </button>
      </div>
      <div className="flex">
        <div className="div-centered login-border">
          <h1>Edit Item</h1>
          <ItemForm
            initialValues={getInitialValues(item)}
            onSubmit={onSubmit}
            error={error}
            buttonText="Edit Item"
          />
        </div>
      </div>
    </ReactModal>
  );
};

const UserTab = ({ user }) => {
  return (
    <table className="table table-dark table-hover">
      <thead>
        <tr>
          <th colSpan="2">Courier information</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Email</td>
          <td>{user.email}</td>
        </tr>
        <tr>
          <td>Phone number</td>
          <td>{user.phoneNumber}</td>
        </tr>
      </tbody>
    </table>
  );
};

const ItemPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);
  const dispatch = useDispatch();

  if (userRole !== role) {
    history.push('/main');
  }

  const { itemId } = useParams();
  const userId = useSelector((state) => state.user.id);
  const registration = useSelector((state) => state.actions.registration);
  const delivery = useSelector((state) => state.actions.delivery);
  const loading = useSelector((state) => state.loading.loading);

  const [isPickupModalOpen, setPickupModalOpen] = useState(false);
  const [pickupLocationId, setPickupLocationId] = useState(null);

  const [isDeliveryModalOpen, setDeliveryModalOpen] = useState(false);
  const [deliveryLocationId, setDeliveryLocationId] = useState(null);

  const [isEditModalOpen, setEditModalOpen] = useState(false);

  const item = useSelector((state) => state.itemList.items).find(
    (i) => i.id === parseInt(itemId),
  );

  if (!item) {
    history.push('/items');
  }

  const fetchActions = () => {
    getActions(userId, itemId, dispatch);
  };

  const fetchLocations = async () => {
    await getLocations();
  };

  const fetchTypes = () => {
    getTypesList(dispatch);
  };

  const fetchItems = () => {
    const callItemsApi = async () => {
      await getItemList(userId, 1, dispatch);
    };
    callItemsApi();
  };

  const userList = useSelector((state) => state.usersList.users);
  const itemCourier = userList ? userList[0] : null;
  const fetchUser = () => {
    getSingleUser(item.courierId, dispatch);
  };

  useEffect(() => {
    fetchActions();
    fetchLocations();
    fetchUser();
    fetchTypes();
  }, []);

  useEffect(() => {
    const post = async () => {
      const dto = {
        startTime: new Date().toISOString(),
        pickupLocation: pickupLocationId,
      };
      try {
        await API.post(`/users/${userId}/items/${itemId}/actions`, dto);
        fetchActions();
        setPickupModalOpen(false);
      } catch (ex) {
        console.log(ex);
      }
    };
    if (pickupLocationId) {
      post();
    }
  }, [pickupLocationId]);

  useEffect(() => {
    const post = async () => {
      const dto = {
        deliveryLocation: deliveryLocationId,
      };
      try {
        await API.post(`/users/${userId}/items/${itemId}/actions`, dto);
        fetchActions();
        setDeliveryModalOpen(false);
      } catch (ex) {
        console.log(ex);
      }
    };
    if (deliveryLocationId) {
      post();
    }
  }, [deliveryLocationId]);

  return (
    <div className="flex">
      <div className="div-centered">
        <ItemData
          item={item}
          registration={registration}
          delivery={delivery}
          loading={loading}
          setPickupModalOpen={setPickupModalOpen}
          setDeliveryModalOpen={setDeliveryModalOpen}
        />
        {itemCourier && <UserTab user={itemCourier} />}
        {item.status === 'Registered' && (
          <button className="button" onClick={() => setEditModalOpen(true)}>
            Edit
          </button>
        )}
        <Loader visible={loading} type="TailSpin" color="Blue" />
      </div>
      <SelectLocationModal
        isModalOpen={isPickupModalOpen}
        setModalOpen={setPickupModalOpen}
        setLocationId={setPickupLocationId}
        buttonText="Select this location"
      />
      <SelectLocationModal
        isModalOpen={isDeliveryModalOpen}
        setModalOpen={setDeliveryModalOpen}
        setLocationId={setDeliveryLocationId}
        buttonText="Select this location"
      />
      <EditItemModal
        isModalOpen={isEditModalOpen}
        setModalOpen={setEditModalOpen}
        userId={userId}
        history={history}
        item={item}
        fetchItems={fetchItems}
      />
    </div>
  );
};

export default ItemPage;
