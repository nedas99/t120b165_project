import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { getActions } from '../services/actionsService';
import Loader from 'react-loader-spinner';
import API from '../services/api/api';
import { getItemList } from '../services/itemService';

const RegistrationTableRows = ({ registration }) => {
  if (!registration) {
    return (
      <tr>
        <td colSpan="2">Pickup location not selected yet</td>
      </tr>
    );
  }

  return (
    <>
      <tr>
        <td>Pickup location's description</td>
        <td>{registration.description}</td>
      </tr>
      <tr>
        <td>Registration date</td>
        <td>{new Date(registration.startTime).toLocaleString('LT-lt')}</td>
      </tr>
    </>
  );
};

const DeliveryTableRows = ({ delivery }) => {
  if (!delivery) {
    return (
      <tr>
        <td colSpan="2">Destination not selected yet</td>
      </tr>
    );
  }

  if (!delivery.startTime) {
    return (
      <tr>
        <td>Delivery location's description</td>
        <td>{delivery.description}</td>
      </tr>
    );
  }

  if (!delivery.endTime) {
    return (
      <>
        <tr>
          <td>Delivery location's description</td>
          <td>{delivery.description}</td>
        </tr>
        <tr>
          <td>Delivery started on</td>
          <td>{new Date(delivery.startTime).toLocaleString('LT-lt')}</td>
        </tr>
      </>
    );
  }

  return (
    <>
      <tr>
        <td>Delivery location's description</td>
        <td>{delivery.description}</td>
      </tr>
      <tr>
        <td>Delivery started on</td>
        <td>{new Date(delivery.startTime).toLocaleString('LT-lt')}</td>
      </tr>
      <tr>
        <td>Delivery ended on</td>
        <td>{new Date(delivery.endTime).toLocaleString('LT-lt')}</td>
      </tr>
    </>
  );
};

const getTableBody = (item, registration, delivery, loading) => {
  return (
    <tbody>
      <tr>
        <td>Id</td>
        <td>{item.id}</td>
      </tr>
      <tr>
        <td>Name</td>
        <td>{item.name}</td>
      </tr>
      <tr>
        <td>Width</td>
        <td>{item.width}</td>
      </tr>
      <tr>
        <td>Length</td>
        <td>{item.length}</td>
      </tr>
      <tr>
        <td>Height</td>
        <td>{item.height}</td>
      </tr>
      <tr>
        <td>Type</td>
        <td>{item.type}</td>
      </tr>
      <tr>
        <td>Status</td>
        <td>{item.status}</td>
      </tr>
      {!loading && (
        <>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <RegistrationTableRows registration={registration} />
          <tr>
            <td></td>
            <td></td>
          </tr>
          <DeliveryTableRows delivery={delivery} />
        </>
      )}
    </tbody>
  );
};

export const ItemData = ({ item, registration, delivery, loading }) => {
  if (!item) {
    return null;
  }

  return (
    <table className="table table-dark" style={{ width: '100%' }}>
      <thead>
        <tr>
          <th>Attribute</th>
          <th>Value</th>
        </tr>
      </thead>
      {getTableBody(item, registration, delivery, loading)}
    </table>
  );
};

const RegisterAsCourierButton = ({
  senderId,
  courierId,
  item,
  delivery,
  history,
  fetchItems,
}) => {
  const getItemDto = (item, courierId) => {
    const dto = {
      name: item.name,
      width: item.width,
      length: item.length,
      height: item.height,
      type: item.typeId,
      status: 2,
      courierId,
    };

    return dto;
  };

  return (
    <button
      className="button full-width"
      onClick={async () => {
        const itemUrl = `/users/${senderId}/items/${item.id}`;
        const itemDto = getItemDto(item, courierId);

        /*
        const { id, locationId } = delivery;
        const dto = {
          location: locationId,
          startTime: new Date().toISOString(),
        };
        const actionUrl = `/users/${senderId}/items/${item.id}/actions/${id}`;*/

        try {
          //await API.put(actionUrl, dto);
          await API.put(itemUrl, itemDto);
          fetchItems();
          history.push('/courier/items');
        } catch (ex) {
          console.log(ex.message);
        }
      }}
    >
      Take this delivery
    </button>
  );
};

const RegisteredItemPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);
  const dispatch = useDispatch();

  if (userRole !== role) {
    history.push('/main');
  }

  const { itemId } = useParams();
  const registration = useSelector((state) => state.actions.registration);
  const delivery = useSelector((state) => state.actions.delivery);
  const loading = useSelector((state) => state.loading.loading);
  const userId = useSelector((state) => state.user.id);

  const item = useSelector((state) => state.itemList.items).find(
    (i) => i.id === parseInt(itemId),
  );

  if (!item) {
    history.push('/items');
  }

  const fetchItems = () => {
    getItemList(userId, 1, dispatch);
  };

  const fetchActions = () => {
    getActions(item.senderId, itemId, dispatch);
  };

  useEffect(() => {
    fetchActions();
  }, []);

  return (
    <div className="flex">
      <div className="div-centered">
        <ItemData
          item={item}
          registration={registration}
          delivery={delivery}
          loading={loading}
        />
        {!loading && registration && delivery && (
          <RegisterAsCourierButton
            senderId={item.senderId}
            courierId={userId}
            item={item}
            delivery={delivery}
            history={history}
            fetchItems={fetchItems}
          />
        )}
        <Loader visible={loading} type="TailSpin" color="Blue" />
      </div>
    </div>
  );
};

const exportObj = {
  ItemData,
  RegisteredItemPage,
};

export default exportObj;
