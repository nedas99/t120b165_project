import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import LocationsMap from '../components/LocationsMap';
import { useSelector } from 'react-redux';
import ReactModal from 'react-modal';
import API from '../services/api/api';
import { getLocations } from '../services/locationService';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

ReactModal.setAppElement('#modal');

const LocationsPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [locationId, setLocationId] = useState(null);

  if (userRole !== role) {
    history.push('/main');
  }

  const fetchLocations = async () => {
    await getLocations();
  };

  useEffect(() => {
    fetchLocations();
  }, []);

  const handleDeleteLocation = async () => {
    try {
      API.setErrorInterceptors();
      await API.delete(`/locations/${locationId}`);
      await fetchLocations();
      setLocationId(null);
      setIsModalOpen(false);
    } catch (ex) {
      console.log(ex.message);
    } finally {
      API.resetErrorInterceptor();
    }
  };

  return (
    <>
      <div className="flex">
        <h1>Locations</h1>
      </div>
      <div className="flex">
        <button
          className="button"
          onClick={() => history.push('/locations/add')}
        >
          Add Location
        </button>
      </div>
      <div className="flex">
        <LocationsMap
          setLocationId={setLocationId}
          buttonText="Delete this location"
          onButtonClick={() => setIsModalOpen(true)}
        />
      </div>
      <ReactModal style={customStyles} isOpen={isModalOpen}>
        <div>
          <h1>Are you sure you want to delete this location?</h1>
          <button className="button" onClick={() => setIsModalOpen(false)}>
            No
          </button>
          <button className="button" onClick={() => handleDeleteLocation()}>
            Yes
          </button>
        </div>
      </ReactModal>
    </>
  );
};

export default LocationsPage;
