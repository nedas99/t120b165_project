import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getUserList } from '../services/usersListService';
import { setPage } from '../store/usersList/actions';
import API from '../services/api/api';
import { useHistory } from 'react-router';

const getUserActions = (user, fetchUsers) => {
  if (user.verified) {
    return null;
  }

  const dto = {
    email: user.email,
    phoneNumber: user.phoneNumber,
    verified: true,
  };

  return (
    <li>
      <button
        title="Verify user"
        onClick={async () => {
          try {
            await API.put(`/users/${user.id}`, dto);
            fetchUsers();
          } catch (err) {
            console.log(err.response);
          }
        }}
      >
        <i class="fas fa-user-check"></i>
      </button>
    </li>
  );
};

const List = ({ users, fetchUsers }) => {
  if (!users) {
    return null;
  }

  return (
    <tbody>
      {users.map((u) => (
        <tr key={u.id}>
          <td>{u.id}</td>
          <td>{u.email}</td>
          <td>{u.phoneNumber}</td>
          <td>{u.role}</td>
          <td>{getUserActions(u, fetchUsers)}</td>
        </tr>
      ))}
    </tbody>
  );
};

const Table = () => {
  const dispatch = useDispatch();
  const page = useSelector((state) => state.usersList.page);
  const previous = useSelector((state) => state.usersList.previous);
  const next = useSelector((state) => state.usersList.next);
  const users = useSelector((state) => state.usersList.users);
  const error = useSelector((state) => state.usersList.error);

  const accessToken = useSelector((state) => state.user.token);

  const fetchUsers = () => {
    const callUsersApi = async () => {
      await getUserList(page, dispatch);
    };
    callUsersApi();
  };

  useEffect(() => {
    fetchUsers();
  }, [page, accessToken]);

  if (error) {
    return <div style={{ color: 'red' }}>{error}</div>;
  }

  if (!users || users.length === 0) {
    return <div className="flex">There are no users</div>;
  }

  return (
    <div>
      <div className="flex">
        <div className="lower-width">
          <table className="table table-dark table-hover">
            <thead>
              <tr>
                <th>Id</th>
                <th>E-mail</th>
                <th>Phone number</th>
                <th>Role</th>
                <th>Actions</th>
              </tr>
            </thead>
            <List users={users} fetchUsers={fetchUsers} />
          </table>
          <div className="margin flex flex-row">
            <div>
              {previous && (
                <button
                  className="button"
                  onClick={() => dispatch(setPage(page - 1))}
                >
                  {'<'}
                </button>
              )}
            </div>
            <div>
              {next && (
                <button
                  className="button"
                  onClick={() => dispatch(setPage(page + 1))}
                >
                  {'>'}
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const UsersListPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  if (userRole !== role) {
    history.push('/main');
  }

  return (
    <>
      <div className="flex">
        <h1>Users</h1>
      </div>
      <div>
        <Table />
      </div>
    </>
  );
};

export default UsersListPage;
