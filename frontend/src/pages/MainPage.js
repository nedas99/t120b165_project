import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const getTabsForRole = (role, tabs) => {
  const forRole = tabs.filter((t) => t.role === role);
  const fullyFiltered = forRole.filter((t) => t.icon);
  return fullyFiltered;
};

const MainPage = ({ tabs }) => {
  const role = useSelector((state) => state.user.role);
  const history = useHistory();

  const filteredTabs = getTabsForRole(role, tabs);

  return (
    <div className="container">
      <div className="row">
        {filteredTabs &&
          filteredTabs.map((t, index) => (
            <div
              key={index}
              className="col-xs-12 col-sm-6 col-md-4 col-xl-3 hover-border"
              //style={{ maxWidth: '200px' }}
              onClick={() => history.push(t.link)}
            >
              <img src={t.icon} alt={t.name} width="100%" />
              <div className="flex" style={{ fontWeight: 'bold' }}>
                {t.name}
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default MainPage;
