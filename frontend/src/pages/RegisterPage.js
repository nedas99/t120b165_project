import React, { useState } from 'react';
import { useHistory } from 'react-router';
import Register from '../components/Register';
import ReactModal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

const RegisterPage = () => {
  const history = useHistory();
  const [modalIsOpen, setModalIsOpen] = useState(false);

  return (
    <div className="flex">
      <div className="div-centered login-border">
        <h1>Register to Shipment Application</h1>
        <Register setModal={setModalIsOpen} />
        <button className="button" onClick={() => history.push('/')}>
          Go to Login Page
        </button>
      </div>
      <ReactModal style={customStyles} isOpen={modalIsOpen}>
        <div>
          <h1>You have successfully registered</h1>
          <button className="button" onClick={() => history.push('/')}>
            Go to Login Page
          </button>
        </div>
      </ReactModal>
    </div>
  );
};

export default RegisterPage;
