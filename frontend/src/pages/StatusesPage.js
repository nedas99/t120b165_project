import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { getStatusList } from '../services/statusService';
import ReactModal from 'react-modal';
import API from '../services/api/api';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '50px',
  },
};

const DeleteModal = ({ isOpen, setIsOpen, fetchStatuses, statusId }) => {
  if (!statusId) {
    return null;
  }

  const handleDelete = async (id) => {
    const url = `/statuses/${id}`;
    try {
      await API.delete(url);
      fetchStatuses();
      setIsOpen(false);
    } catch (ex) {
      console.log(ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isOpen}>
      <div>
        <h1>Are you sure you want to delete this status?</h1>
        <button className="button" onClick={() => setIsOpen(false)}>
          No
        </button>
        <button className="button" onClick={() => handleDelete(statusId)}>
          Yes
        </button>
      </div>
    </ReactModal>
  );
};

const EditModal = ({
  isOpen,
  setIsOpen,
  fetchStatuses,
  statusId,
  name,
  setName,
}) => {
  const [error, setError] = useState('');

  if (!statusId) {
    return null;
  }

  const handleSubmit = async () => {
    if (!name || !name.trim().length) {
      setError('Empty field');
      return;
    }
    const dto = { name };
    const url = `/statuses/${statusId}`;
    try {
      await API.put(url, dto);
      fetchStatuses();
      setName('');
      setIsOpen(false);
    } catch (ex) {
      setError('Error while editing status' + ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isOpen}>
      <button className="button" onClick={() => setIsOpen(false)}>
        Close
      </button>
      <p className="margin">Enter status value:</p>
      <input value={name} onChange={(e) => setName(e.target.value)} />
      {error && <div style={{ color: 'red' }}>{error}</div>}
      <button className="margin button" onClick={() => handleSubmit()}>
        Edit status
      </button>
    </ReactModal>
  );
};

const CreateModal = ({ isOpen, setIsOpen, fetchStatuses }) => {
  const [name, setName] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = async () => {
    if (!name || !name.trim().length) {
      setError('Empty field');
      return;
    }
    const dto = { name };
    const url = '/statuses';
    try {
      await API.post(url, dto);
      fetchStatuses();
      setName('');
      setIsOpen(false);
    } catch (ex) {
      setError('Error while creating status' + ex.message);
    }
  };

  return (
    <ReactModal style={customStyles} isOpen={isOpen}>
      <button className="button" onClick={() => setIsOpen(false)}>
        Close
      </button>
      <p className="margin">Enter status value:</p>
      <input value={name} onChange={(e) => setName(e.target.value)} />
      {error && <div style={{ color: 'red' }}>{error}</div>}
      <button className="margin button" onClick={() => handleSubmit()}>
        Create new
      </button>
    </ReactModal>
  );
};

const TableBody = ({
  statuses,
  setStatusId,
  handleOpenEditModal,
  setDeleteOpen,
}) => {
  if (!statuses) {
    return null;
  }

  const getButtons = (id) => {
    return (
      <>
        <li>
          <button
            onClick={() => {
              setStatusId(id);
              handleOpenEditModal();
            }}
          >
            <i class="fas fa-pencil-alt"></i>
          </button>
        </li>
        <li>
          <button
            onClick={() => {
              setStatusId(id);
              setDeleteOpen(true);
            }}
          >
            <i class="fas fa-ban"></i>
          </button>
        </li>
      </>
    );
  };

  return (
    <tbody>
      {statuses.map((s) => (
        <tr key={s.id}>
          <td>{s.id}</td>
          <td>{s.name}</td>
          <td>{getButtons(s.id)}</td>
        </tr>
      ))}
    </tbody>
  );
};

const Table = ({ setStatusId, handleOpenEditModal, list, setDeleteOpen }) => {
  const error = useSelector((state) => state.status.error);

  if (error) {
    return <div style={{ color: 'red' }}>{error}</div>;
  }

  if (!list || list.length === 0) {
    return <div className="flex">There are no statuses</div>;
  }

  return (
    <div className="flex">
      <div className="lower-width">
        <table className="table table-dark table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <TableBody
            statuses={list}
            setStatusId={setStatusId}
            handleOpenEditModal={handleOpenEditModal}
            setDeleteOpen={setDeleteOpen}
          />
        </table>
      </div>
    </div>
  );
};

const StatusesPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  if (userRole !== role) {
    history.push('/main');
  }

  const dispatch = useDispatch();
  const fetchStatuses = () => {
    getStatusList(dispatch);
  };

  const [createOpen, setCreateOpen] = useState(false);
  const [deleteOpen, setDeleteOpen] = useState(false);

  const [editOpen, setEditOpen] = useState(false);
  const [statusId, setStatusId] = useState(0);
  const [name, setName] = useState('');

  const list = useSelector((state) => state.status.list);

  useEffect(() => {
    fetchStatuses();
  }, []);

  const handleOpenEditModal = () => {
    if (statusId) {
      const status = list.find((s) => s.id === statusId);
      status && setName(status.name);
      setEditOpen(true);
    }
  };

  useEffect(() => {
    if (statusId) {
      const status = list.find((s) => s.id === statusId);
      status && setName(status.name);
    }
  }, [statusId]);

  return (
    <>
      <div className="flex">
        <h1>Statuses</h1>
      </div>
      <div className="flex">
        <button className="button" onClick={() => setCreateOpen(true)}>
          Create new
        </button>
      </div>
      <div>
        <Table
          setStatusId={setStatusId}
          list={list}
          handleOpenEditModal={handleOpenEditModal}
          setDeleteOpen={setDeleteOpen}
        />
      </div>
      <CreateModal
        isOpen={createOpen}
        setIsOpen={setCreateOpen}
        fetchStatuses={fetchStatuses}
      />
      <EditModal
        isOpen={editOpen}
        setIsOpen={setEditOpen}
        fetchStatuses={fetchStatuses}
        statusId={statusId}
        name={name}
        setName={setName}
      />
      <DeleteModal
        isOpen={deleteOpen}
        setIsOpen={setDeleteOpen}
        fetchStatuses={fetchStatuses}
        statusId={statusId}
      />
    </>
  );
};

export default StatusesPage;
