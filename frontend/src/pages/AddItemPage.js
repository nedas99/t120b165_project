import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import ItemForm from '../components/ItemForm';
import API from '../services/api/api';

const AddItemPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  if (userRole !== role) {
    history.push('/main');
  }

  const [error, setError] = useState('');
  const userId = useSelector((state) => state.user.id);

  const onSubmit = async (values) => {
    console.log(values);
    const dto = {
      ...values,
      status: 1,
    };

    try {
      await API.post(`/users/${userId}/items`, dto);
      history.push('/items');
    } catch (err) {
      setError(err.response);
    }
  };

  return (
    <div className="flex">
      <div className="div-centered login-border">
        <h1>Create item</h1>
        <ItemForm
          initialValues={{
            name: '',
            width: 1,
            length: 1,
            height: 1,
            type: 1,
          }}
          error={error}
          onSubmit={onSubmit}
          buttonText="Create Item"
        />
        <button className="button" onClick={() => history.push('/items')}>
          Go to Item Page
        </button>
      </div>
    </div>
  );
};

export default AddItemPage;
