import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { getAllItems } from '../services/itemService';
import { setPage } from '../store/items/actions';

const List = ({ items, history }) => {
  if (!items) {
    return null;
  }

  const getDimensions = (item) => {
    return `${item.width} : ${item.length} : ${item.height}`;
  };

  return (
    <tbody>
      {items.map((i) => (
        <tr key={i.id} onClick={() => history.push(`/admin/items/${i.id}`)}>
          <td>{i.id}</td>
          <td>{i.name}</td>
          <td>{getDimensions(i)}</td>
          <td>{i.type}</td>
          <td>{i.status}</td>
        </tr>
      ))}
    </tbody>
  );
};

const Table = ({ history }) => {
  const dispatch = useDispatch();
  const page = useSelector((state) => state.itemList.page);
  const previous = useSelector((state) => state.itemList.previous);
  const next = useSelector((state) => state.itemList.next);
  const items = useSelector((state) => state.itemList.items);
  const error = useSelector((state) => state.itemList.error);

  const accessToken = useSelector((state) => state.user.token);

  const fetchItems = () => {
    const callItemsApi = async () => {
      await getAllItems(page, dispatch);
    };
    callItemsApi();
  };

  useEffect(() => {
    fetchItems();
  }, [page, accessToken]);

  if (error) {
    return <div style={{ color: 'red' }}>{error}</div>;
  }

  if (!items || items.length === 0) {
    return <div className="flex">There are no items</div>;
  }

  return (
    <div>
      <div className="flex">
        <div className="lower-width">
          <table className="table table-dark table-hover">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Dimensions</th>
                <th>Type</th>
                <th>Status</th>
              </tr>
            </thead>
            <List items={items} history={history} />
          </table>
          <div className="margin flex flex-row">
            <div>
              {previous && (
                <button
                  className="button"
                  onClick={() => dispatch(setPage(page - 1))}
                >
                  {'<'}
                </button>
              )}
            </div>
            <div>
              {next && (
                <button
                  className="button"
                  onClick={() => dispatch(setPage(page + 1))}
                >
                  {'>'}
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const AdminItemListPage = ({ role }) => {
  const history = useHistory();
  const userRole = useSelector((state) => state.user.role);

  if (userRole !== role) {
    history.push('/main');
  }

  return (
    <>
      <div className="flex">
        <h1>All Items</h1>
      </div>
      <div>
        <Table history={history} />
      </div>
    </>
  );
};

export default AdminItemListPage;
