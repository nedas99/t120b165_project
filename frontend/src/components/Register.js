import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { useHistory } from 'react-router';
import CustomField from './CustomField';
import CustomDropdown from './CustomDropdown';
import API from '../services/api/api';

const initialValues = {
  email: '',
  password: '',
  phoneNumber: '',
  role: 1,
};

const validationSchema = yup.object({
  email: yup.string().email('Invalid email').required('Enter your email'),
  password: yup
    .string()
    .required('Enter your password')
    .min(6, 'Password must be at least 6 symbols'),
  phoneNumber: yup
    .string()
    .required('Enter your phone number')
    .matches(/^\+[0-9]+$/, 'Invalid phone number'),
  role: yup.number().required('Pick your role'),
});

const roleOptions = [
  {
    text: 'Courier',
    value: 1,
  },
  {
    text: 'Sender',
    value: 2,
  },
];

const Register = ({ setModal }) => {
  const history = useHistory();
  const [error, setError] = useState('');

  const onSubmit = async (values) => {
    const dto = { ...values, role: parseInt(values.role) };
    console.log(dto);
    try {
      API.resetErrorInterceptor();
      await API.post('/users', dto);
      setModal(true);
    } catch (err) {
      if (
        err.response.data !== undefined &&
        err.response.data.email !== undefined
      ) {
        setError(err.response.data.email[0]);
      }
    } finally {
      API.setErrorInterceptors();
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <CustomField type="text" name="email" labelText="Email" />
        <CustomField type="password" name="password" labelText="Password" />
        <CustomField
          type="phoneNumber"
          name="phoneNumber"
          labelText="Phone Number"
        />
        <CustomDropdown
          type="role"
          name="role"
          labelText="Role"
          options={roleOptions}
        />
        {error && <div className="error">{error}</div>}
        <div className="margin">
          <button
            type="submit"
            className="button"
            onClick={() => history.push('/register')}
          >
            Register
          </button>
        </div>
      </Form>
    </Formik>
  );
};

export default Register;
