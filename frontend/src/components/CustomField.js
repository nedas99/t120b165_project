import React from 'react';
import { Field, ErrorMessage } from 'formik';

const CustomField = ({ type, name, labelText }) => {
  const baseProps = {
    className: 'field',
    type,
    id: name,
    name,
    placeholder: labelText,
  };

  const props =
    type === 'number'
      ? {
          ...baseProps,
          min: 1,
        }
      : baseProps;

  return (
    <div className="margin flex flex-column">
      <Field {...props} />
      <ErrorMessage
        name={name}
        render={(err) => <div className="error">{err}</div>}
      />
    </div>
  );
};

export default CustomField;
