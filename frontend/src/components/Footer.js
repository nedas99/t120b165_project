import React from 'react';

const style = {
  position: 'fixed',
  left: 0,
  bottom: 0,
  width: '100%',
  backgroundColor: 'black',
  color: 'white',
  textAlign: 'center',
  fontWeight: 'bold',
};

const Footer = () => {
  return (
    <div>
      <footer style={style}>Nedas Šimoliūnas IFF-8/2</footer>
    </div>
  );
};

export default Footer;
