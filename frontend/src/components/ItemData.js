import React from 'react';

const RegistrationTableRows = ({ registration, setModalOpen }) => {
  if (!registration) {
    return (
      <tr>
        <td colSpan="2">
          <button
            className="button full-width"
            onClick={() => setModalOpen(true)}
          >
            Select pickup
          </button>
        </td>
      </tr>
    );
  }

  return (
    <>
      <tr>
        <td>Pickup location's description</td>
        <td>{registration.description}</td>
      </tr>
      <tr>
        <td>Registration date</td>
        <td>{new Date(registration.startTime).toLocaleString('LT-lt')}</td>
      </tr>
    </>
  );
};

const DeliveryTableRows = ({ delivery, setModalOpen }) => {
  if (!delivery) {
    return (
      <tr>
        <td colSpan="2">
          <button
            className="button full-width"
            onClick={() => setModalOpen(true)}
          >
            Select destination
          </button>
        </td>
      </tr>
    );
  }

  if (!delivery.startTime) {
    return (
      <tr>
        <td>Delivery location's description</td>
        <td>{delivery.description}</td>
      </tr>
    );
  }

  if (!delivery.endTime) {
    return (
      <>
        <tr>
          <td>Delivery location's description</td>
          <td>{delivery.description}</td>
        </tr>
        <tr>
          <td>Delivery started on</td>
          <td>{new Date(delivery.startTime).toLocaleString('LT-lt')}</td>
        </tr>
      </>
    );
  }

  return (
    <>
      <tr>
        <td>Delivery location's description</td>
        <td>{delivery.description}</td>
      </tr>
      <tr>
        <td>Delivery started on</td>
        <td>{new Date(delivery.startTime).toLocaleString('LT-lt')}</td>
      </tr>
      <tr>
        <td>Delivery ended on</td>
        <td>{new Date(delivery.endTime).toLocaleString('LT-lt')}</td>
      </tr>
    </>
  );
};

const getTableBody = (
  item,
  registration,
  delivery,
  loading,
  setModalOpen,
  setDeliveryModalOpen,
) => {
  return (
    <tbody>
      <tr>
        <td>Id</td>
        <td>{item.id}</td>
      </tr>
      <tr>
        <td>Name</td>
        <td>{item.name}</td>
      </tr>
      <tr>
        <td>Width</td>
        <td>{item.width}</td>
      </tr>
      <tr>
        <td>Length</td>
        <td>{item.length}</td>
      </tr>
      <tr>
        <td>Height</td>
        <td>{item.height}</td>
      </tr>
      <tr>
        <td>Type</td>
        <td>{item.type}</td>
      </tr>
      <tr>
        <td>Status</td>
        <td>{item.status}</td>
      </tr>
      {!loading && (
        <>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <RegistrationTableRows
            registration={registration}
            setModalOpen={setModalOpen}
          />
          <tr>
            <td></td>
            <td></td>
          </tr>
          <DeliveryTableRows
            delivery={delivery}
            setModalOpen={setDeliveryModalOpen}
          />
        </>
      )}
    </tbody>
  );
};

const ItemData = ({
  item,
  registration,
  delivery,
  loading,
  setPickupModalOpen,
  setDeliveryModalOpen,
}) => {
  if (!item) {
    return null;
  }

  return (
    <table className="table table-dark w-auto">
      <thead>
        <tr>
          <th>Attribute</th>
          <th>Value</th>
        </tr>
      </thead>
      {getTableBody(
        item,
        registration,
        delivery,
        loading,
        setPickupModalOpen,
        setDeliveryModalOpen,
      )}
    </table>
  );
};

export default ItemData;
