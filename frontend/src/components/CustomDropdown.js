import React from 'react';
import { Field, ErrorMessage } from 'formik';

const CustomDropdown = ({ type, name, labelText, options }) => {
  return (
    <div className="margin flex flex-column">
      <Field
        as="select"
        className="field"
        type={type}
        id={name}
        name={name}
        placeholder={labelText}
      >
        {options.map((o, index) => (
          <option key={index} value={o.value}>
            {o.text}
          </option>
        ))}
      </Field>
      <ErrorMessage
        name={name}
        render={(err) => <div className="error">{err}</div>}
      />
    </div>
  );
};

export default CustomDropdown;
