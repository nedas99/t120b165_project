import React, { useState, useEffect } from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import loginService from '../services/loginService';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import CustomField from './CustomField';
import Loader from 'react-loader-spinner';

const initialValues = {
  email: '',
  password: '',
};

const validationSchema = yup.object({
  email: yup.string().email('Invalid email').required('Enter your email'),
  password: yup
    .string()
    .required('Enter your password')
    .min(6, 'Password must be at least 6 symbols'),
});

const Login = () => {
  const [error, setError] = useState('');
  const dispatch = useDispatch();
  const role = useSelector((state) => state.user.role);
  const loading = useSelector((state) => state.loading.loading);
  const history = useHistory();

  const onSubmit = async (values) => {
    loginService(values, dispatch).catch(() => {
      setError('Failed to login');
    });
  };

  useEffect(() => {
    role && history.push('/main');
  }, [role]);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <CustomField type="text" name="email" labelText="Email" />
        <CustomField type="password" name="password" labelText="Password" />
        {error && <div className="error">{error}</div>}
        <Loader
          visible={loading}
          type="TailSpin"
          color="Blue"
          width={30}
          height={30}
        />
        <div className="margin">
          <button className="button" type="submit">
            Login
          </button>
          <button className="button" onClick={() => history.push('/register')}>
            Register
          </button>
        </div>
      </Form>
    </Formik>
  );
};

export default Login;
