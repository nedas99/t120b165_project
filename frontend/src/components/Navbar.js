import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import logoutService from '../services/logoutService';
import { useDispatch, useSelector } from 'react-redux';

const Navbar = ({ tabs, setMenuOpen }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const role = useSelector((state) => state.user.role);

  const handleLogout = async () => {
    await logoutService(dispatch);
  };

  useEffect(() => {
    !role && history.push('/');
  }, [role]);

  return (
    <header>
      <ul>
        {role && (
          <>
            <li>
              <button onClick={() => setMenuOpen(true)}>
                <i class="fas fa-bars"></i>
              </button>
            </li>
            <li style={{ float: 'right' }}>
              <button onClick={() => handleLogout()}>Logout</button>
            </li>
          </>
        )}
      </ul>
    </header>
  );
};

export default Navbar;
