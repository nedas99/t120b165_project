import React, { useEffect } from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import CustomField from './CustomField';
import CustomDropdown from './CustomDropdown';
import { getTypesList } from '../services/typesService';
import { useDispatch, useSelector } from 'react-redux';

/*const initialValues = {
  name: '',
  width: 1,
  length: 1,
  height: 1,
  type: 1,
};*/

const validationSchema = yup.object({
  name: yup.string().required('Enter items name'),
  width: yup.number().min(1, 'Width has to be a positive integer'),
  length: yup.number().min(1, 'Length has to be a positive integer'),
  height: yup.number().min(1, 'Height has to be a positive integer'),
  type: yup.number().required('Pick item type'),
});

const ItemForm = ({ initialValues, onSubmit, error, buttonText }) => {
  const dispatch = useDispatch();

  const types = useSelector((state) => state.types.list);

  useEffect(() => {
    getTypesList(dispatch);
  }, []);

  const getTypes = () => {
    if (!types) {
      return [];
    }

    return types.map((t) => ({
      text: t.name,
      value: t.id,
    }));
  };

  /*const onSubmit = async (values) => {
    console.log(values);
    const dto = {
      ...values,
      status: 1,
    };
    console.log(dto);

    try {
      await API.post(`/users/${userId}/items`, dto);
      history.push('/items');
    } catch (err) {
      setError(err.response);
    }
  };*/

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <CustomField type="text" name="name" labelText="Name" />
        <CustomField type="number" name="width" labelText="Width" />
        <CustomField type="number" name="length" labelText="Length" />
        <CustomField type="number" name="height" labelText="Height" />
        <CustomDropdown
          type="type"
          name="type"
          labelText="Type"
          options={getTypes()}
        />
        {error && <div className="error">{error}</div>}
        <div className="margin">
          <button type="submit" className="button">
            {buttonText}
          </button>
        </div>
      </Form>
    </Formik>
  );
};

export default ItemForm;
