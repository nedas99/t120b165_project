import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const getSidebarTabsForRole = (role, mainMenuNavbarTabs) => {
  if (!role) {
    return [];
  }

  const tabs = mainMenuNavbarTabs.filter(
    (t) => (t.role === 'All' || t.role === role) && t.isTab,
  );
  return tabs;
};

/*
<button className="menu-item">1</button>
      <button className="menu-item">2</button>
      <button className="menu-item">3</button>
*/

const Sidebar = ({ open, setOpen, tabs }) => {
  const history = useHistory();
  const role = useSelector((state) => state.user.role);
  if (!role) {
    return null;
  }

  const sidebarTabs = getSidebarTabsForRole(role, tabs);

  if (!sidebarTabs || sidebarTabs.length === 0) {
    return null;
  }

  return (
    <Menu isOpen={open} onClose={() => setOpen(false)}>
      <button
        className="menu-item"
        onClick={() => {
          setOpen(false);
          history.push('/main');
        }}
      >
        Main menu
      </button>
      {sidebarTabs.map((t, index) => (
        <button
          key={index}
          className="menu-item"
          onClick={() => {
            setOpen(false);
            history.push(t.link);
          }}
        >
          {t.name}
        </button>
      ))}
    </Menu>
  );
};

export default Sidebar;
