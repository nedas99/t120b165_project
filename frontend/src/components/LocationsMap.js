import React from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import markerIcon from '../images/map-pin.svg';
import { useSelector } from 'react-redux';

const LocationsMap = ({ setLocationId, buttonText, onButtonClick }) => {
  // Settings to show whole Lithuania on the map:
  const center = [55.2, 24];
  const zoom = 7;

  const icon = L.icon({
    iconUrl: markerIcon,
    iconSize: [50, 50],
    iconAnchor: [25, 50],
  });

  const locations = useSelector((state) => state.locations.locations);

  return (
    <MapContainer
      center={center}
      zoom={zoom}
      scrollWheelZoom={true}
      style={{ height: 500, width: '50%', zIndex: 0 }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {locations.map((location, idx) => (
        <Marker
          key={idx}
          position={[location.latitude, location.longitude]}
          icon={icon}
        >
          <Popup>
            <div>{location.description}</div>
            <div>
              <li>
                <button
                  title={buttonText}
                  onClick={() => {
                    setLocationId(location.id);
                    if (onButtonClick) {
                      onButtonClick();
                    }
                  }}
                >
                  {buttonText === 'Delete this location' ? (
                    <i class="fas fa-ban"></i>
                  ) : (
                    <i class="fas fa-check-circle" />
                  )}
                </button>
              </li>
            </div>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};

export default LocationsMap;
