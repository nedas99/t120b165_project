import React from 'react';
import { MapContainer, TileLayer, Marker, MapConsumer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import markerIcon from '../images/map-pin.svg';

const LocationPicker = ({ location, setLocation, isModalOpen }) => {
  const center = [55.2, 24];
  const zoom = 7;

  const icon = L.icon({
    iconUrl: markerIcon,
    iconSize: [50, 50],
    iconAnchor: [25, 50],
  });

  if (isModalOpen) {
    return null;
  }

  return (
    <MapContainer
      center={center}
      zoom={zoom}
      scrollWheelZoom={true}
      style={{ height: 500, width: '50%' }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <MapConsumer>
        {(map) => {
          map.on('click', (e) => {
            const { lat, lng } = e.latlng;
            setLocation({ latitude: lat, longitude: lng });
          });
          return null;
        }}
      </MapConsumer>
      {location && (
        <Marker
          icon={icon}
          position={[location.latitude, location.longitude]}
        ></Marker>
      )}
    </MapContainer>
  );
};

export default LocationPicker;
