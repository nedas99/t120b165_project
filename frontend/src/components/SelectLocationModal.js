import React from 'react';
import ReactModal from 'react-modal';
import LocationsMap from './LocationsMap';

const SelectLocationModal = ({
  isModalOpen,
  setModalOpen,
  setLocationId,
  buttonText,
}) => {
  return (
    <ReactModal isOpen={isModalOpen}>
      <div className="flex">
        <button className="button" onClick={() => setModalOpen(false)}>
          Close
        </button>
      </div>
      <div className="flex">
        <LocationsMap setLocationId={setLocationId} buttonText={buttonText} />
      </div>
    </ReactModal>
  );
};

export default SelectLocationModal;
