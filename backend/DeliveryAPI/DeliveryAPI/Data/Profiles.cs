﻿using AutoMapper;
using DeliveryAPI.Data.Entities;

namespace DeliveryAPI.Data
{
    public class Profiles : Profile
    {
        public Profiles()
        {
            CreateMap<StatusDTO, Status>();
            CreateMap<TypeDTO, Data.Entities.Type>();
            CreateMap<LocationDTO, Location>();

            CreateMap<CreateUserDTO, User>()
                .ForMember(dest => dest.Password,
                           opt => opt.Ignore());

            CreateMap<UpdateUserDTO, User>();

            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.Role,
                           opt => opt.MapFrom(src => src.Role.ToString()));

            CreateMap<UpdateItemDTO, Item>()
                .ForMember(dest => dest.Type,
                           opt => opt.Ignore())
                .ForMember(dest => dest.Status,
                           opt => opt.Ignore())

                .ForMember(dest => dest.CourierId,
                           opt => opt.Condition((src) => src.CourierId != null))

                .ForMember(dest => dest.TypeId,
                           opt => opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.StatusId,
                           opt => opt.MapFrom(src => src.Status));

            CreateMap<CreateItemDTO, Item>()
                .ForMember(dest => dest.Type,
                           opt => opt.Ignore())
                .ForMember(dest => dest.Status,
                           opt => opt.Ignore())

                .ForMember(dest => dest.TypeId,
                           opt => opt.MapFrom(src => src.Type));


            CreateMap<Item, ItemDTO>()
                .ForMember(dest => dest.Type,
                           opt => opt.MapFrom(src => src.Type.Name))
                .ForMember(dest => dest.Status,
                           opt => opt.MapFrom(src => src.Status.Name));


            CreateMap<CreateActionDTO, Action>();

            CreateMap<UpdateActionDTO, Action>()
                .ForMember(dest => dest.Location,
                           opt => opt.Ignore())
                .ForMember(dest => dest.StartTime,
                           opt => opt.Condition((src) => src.StartTime != null))
                .ForMember(dest => dest.EndTime,
                           opt => opt.Condition((src) => src.EndTime != null));

            CreateMap<Action, ActionDTO>()
                .ForMember(dest => dest.Latitude,
                           opt => opt.MapFrom(src => src.Location.Latitude))
                .ForMember(dest => dest.Longitude,
                           opt => opt.MapFrom(src => src.Location.Longitude))
                .ForMember(dest => dest.Description,
                           opt => opt.MapFrom(src => src.Location.Description));
        }
    }
}