﻿namespace DeliveryAPI.Data.Entities
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }

        public int TypeId { get; set; }
        public Type Type { get; set; }
        public int? CourierId { get; set; }
        public User Courier { get; set; }
        public int? SenderId { get; set; }
        public User Sender { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
        public int? RegistrationId { get; set; }
        public Action Registration { get; set; }
        public int? DeliveryId { get; set; }
        public Action Delivery { get; set; }
    }
}
