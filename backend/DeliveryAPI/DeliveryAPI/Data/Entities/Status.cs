﻿namespace DeliveryAPI.Data.Entities
{
    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Retrievable { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
