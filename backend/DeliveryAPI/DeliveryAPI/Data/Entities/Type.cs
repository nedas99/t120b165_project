﻿namespace DeliveryAPI.Data.Entities
{
    public class Type
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
