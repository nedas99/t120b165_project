﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeliveryAPI.Data.Entities
{
    public enum Role
    {
        Admin, Courier, Sender
    }

    public class User
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public Role Role { get; set; }
        public bool Verified { get; set; }

        [InverseProperty("Sender")]
        public ICollection<Item> SenderItems { get; set; }

        [InverseProperty("Courier")]
        public ICollection<Item> CourierItems { get; set; }

        public IEnumerable<RefreshToken> RefreshTokens { get; set; }
    }
}
