﻿using System;

namespace DeliveryAPI.Data.Entities
{
    public class Action
    {
        public int Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public int LocationId { get; set; }
        public Location Location { get; set; }
    }
}
