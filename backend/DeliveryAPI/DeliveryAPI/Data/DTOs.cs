﻿using System.ComponentModel.DataAnnotations;
using DeliveryAPI.Data.Entities;
using System;
using System.Collections.Generic;

namespace DeliveryAPI.Data
{
    public record StatusDTO([Required] string Name);
    public record TypeDTO([Required] string Name);
    public record LocationDTO(
        [Required] double? Longitude,
        [Required] double? Latitude,
        [Required] string Description
    );

    public record CreateUserDTO(
        [Required][EmailAddress] string Email,
        [Required] string PhoneNumber,
        [Required][Range(0, 2)] Role Role,
        [Required] string Password
    );

    public record UpdateUserDTO(
        [Required][EmailAddress] string Email,
        [Required] string PhoneNumber,
        [Required] bool? Verified
    );

    public record UserDTO(
        int Id,
        string Email,
        string PhoneNumber,
        string Role,
        bool Verified
    );

    public record CreateItemDTO(
        [Required] string Name,
        [Required][Range(1, int.MaxValue)] int Width,
        [Required][Range(1, int.MaxValue)] int Length,
        [Required][Range(1, int.MaxValue)] int Height,
        [Required] int? Type
    );

    public record UpdateItemDTO(
        [Required] string Name,
        [Required][Range(1, int.MaxValue)] int Width,
        [Required][Range(1, int.MaxValue)] int Length,
        [Required][Range(1, int.MaxValue)] int Height,
        [Required] int? Type,
        [Required] int? Status,
        int? CourierId
    );

    public record ItemDTO(
        int Id,
        int? SenderId,
        int? CourierId,
        string Name,
        int Width,
        int Length,
        int Height,
        int TypeId,
        int StatusId,
        string Type,
        string Status
    );

    public record ItemList(
        bool PreviousPage,
        bool NextPage,
        int Count,
        IEnumerable<ItemDTO> Items
    );

    public record UserList(
        bool PreviousPage,
        bool NextPage,
        int Count,
        IEnumerable<UserDTO> Users
    );

    public record CreateActionDTO(
        DateTime? StartTime,
        int? PickupLocation,
        int? DeliveryLocation
    );

    public record UpdateActionDTO(
        DateTime? StartTime,
        DateTime? EndTime,
        [Required] int? Location
    );

    public class ActionDTO
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public ActionDTO() { }
    }

    public record LoginDTO(
        [Required] string Email, 
        [Required] string Password
    );

    public record LoginResponseDTO(
        string JwtToken,
        string Role,
        int Id
    );

    public record RevokeTokenRequest(string Token);
}
