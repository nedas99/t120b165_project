﻿using System.Linq;
using DeliveryAPI.Data.Entities;
using System;
using Type = DeliveryAPI.Data.Entities.Type;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DeliveryAPI.Data
{
    public class DataContextSeed
    {
        private readonly DataContext context;

        public DataContextSeed(DataContext context)
        {
            this.context = context;
        }

        public void Fill()
        {
            if (!context.Statuses.Any())
                FillStatuses(context);

            if (!context.Types.Any())
                FillTypes(context);

            if (!context.Locations.Any())
                FillLocations(context);

            if (!context.Users.Any())
                FillUsers(context);

            /*if (!context.Actions.Any())
                FillActions(context);*/

            /*if (!context.Items.Any())
                FillItems(context);*/
        }

        private void FillStatuses(DataContext context)
        {
            var statuses = new List<Status>
            {
                new Status { Id = 1, Name = "Registered", Retrievable = false },
                new Status { Id = 2, Name = "Assigned", Retrievable = false },
                new Status { Id = 3, Name = "On route", Retrievable = false },
                new Status { Id = 5, Name = "Lost", Retrievable = true },
                new Status { Id = 6, Name = "Delivered", Retrievable = false },
                new Status { Id = 4, Name = "Canceled", Retrievable = true },
            };
            
            context.Database.OpenConnection();
            try
            {
                context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Status ON");
                
                foreach (var s in statuses)
                {
                    context.Statuses.Add(s);
                }

                context.SaveChanges();
                context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Status OFF");
            }
            finally
            {
                context.Database.CloseConnection();
            }
        }

        private void FillTypes(DataContext context)
        {
            context.Types.Add(new Type { Name = "Box" });
            context.Types.Add(new Type { Name = "Fluid canister" });
            context.Types.Add(new Type { Name = "Food" });
            context.Types.Add(new Type { Name = "Fragile" });

            context.SaveChanges();
        }

        private void FillLocations(DataContext context)
        {
            context.Locations.Add(new Location { Latitude = 54.9238, Longitude = 23.9364, Description = "Varduvos g. 4, Kaunas" });
            context.Locations.Add(new Location { Latitude = 54.6868, Longitude = 25.2815, Description = "Gedimino pr. 9, Vilnius" });
            context.Locations.Add(new Location { Latitude = 55.6915, Longitude = 21.1651, Description = "Dubysos g. 31A, Klaipėda" });
            context.Locations.Add(new Location { Latitude = 55.9083, Longitude = 23.2598, Description = "Aido g. 8, Šiauliai" });
            context.Locations.Add(new Location { Latitude = 55.9278, Longitude = 23.3083, Description = "Saulės miestas, Šiauliai" });
            context.Locations.Add(new Location { Latitude = 55.9431, Longitude = 23.3286, Description = "Tilžė, Šiauliai" });
            context.Locations.Add(new Location { Latitude = 55.7103, Longitude = 21.135, Description = "Meridianas, Klaipėda" });
            context.Locations.Add(new Location { Latitude = 55.982, Longitude = 22.2497, Description = "Naujoji g. 10, Telšiai" });
            context.Locations.Add(new Location { Latitude = 54.9387, Longitude = 23.8948, Description = "Mega, Kaunas" });
            context.Locations.Add(new Location { Latitude = 54.8923, Longitude = 23.9175, Description = "Akropolis, Kaunas" });
            context.Locations.Add(new Location { Latitude = 54.6936, Longitude = 25.2759, Description = "CUP, Vilnius" });
            context.Locations.Add(new Location { Latitude = 55.7305, Longitude = 24.3037, Description = "Depo, Panevėžys" });
            context.Locations.Add(new Location { Latitude = 55.4994, Longitude = 25.6029, Description = "Aušros g. 11, Utena" });
            context.Locations.Add(new Location { Latitude = 55.2567, Longitude = 22.2977, Description = "Žemaitės g. 30, Tauragė" });
            context.Locations.Add(new Location { Latitude = 54.5663, Longitude = 23.3583, Description = "Kauno g. 71, Marijampolė" });
            context.Locations.Add(new Location { Latitude = 54.3953, Longitude = 24.0412, Description = "Senoji g. 5, Alytus" });

            context.SaveChanges();
        }

        private void FillUsers(DataContext context)
        {
            context.Users.Add(new User { Email = "sender@gmail.com", PhoneNumber = "+37066666666", Role = Role.Sender, Password = "sodlNm7pomrKvBq7OpUYzHQ5IDJoGzrz163xpFkVMUwNMxf7", Verified = true });
            context.Users.Add(new User { Email = "admin@gmail.com", PhoneNumber = "+37012345678", Role = Role.Admin, Password = "sodlNm7pomrKvBq7OpUYzHQ5IDJoGzrz163xpFkVMUwNMxf7", Verified = true });
            context.Users.Add(new User { Email = "courier@gmail.com", PhoneNumber = "+37098765432", Role = Role.Courier, Password = "sodlNm7pomrKvBq7OpUYzHQ5IDJoGzrz163xpFkVMUwNMxf7", Verified = false });

            context.SaveChanges();
        }

        private void FillActions(DataContext context)
        {
            context.Actions.Add(new Entities.Action { LocationId = 1, StartTime = DateTime.Now });

            context.SaveChanges();
        }

        private void FillItems(DataContext context)
        {
            context.Items.Add(new Item { RegistrationId = 1, Height = 1, Length = 1, Width = 1, Name = "milk", SenderId = 1, StatusId = 1, TypeId = 1});

            context.SaveChanges();
        }
    }
}
