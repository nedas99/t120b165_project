﻿using DeliveryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryAPI.Data.Repositories
{
    public interface ITypeRepository
    {
        Task<Type> Add(Type type);
        Task Delete(Type type);
        Task<Type> Get(int id);
        Task<IEnumerable<Type>> GetAll();
        Task Update(Type type);
    }

    public class TypeRepository : ITypeRepository
    {
        private readonly DataContext context;

        public TypeRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Type>> GetAll()
        {
            var types = await context.Types
                .OrderBy(t => t.Id)
                .ToListAsync();

            return types;
        }

        public async Task<Type> Get(int id)
        {
            var type = await context.Types
                .FirstOrDefaultAsync(s => s.Id == id);

            return type;
        }

        public async Task<Type> Add(Type type)
        {
            context.Types.Add(type);
            await context.SaveChangesAsync();
            return type;
        }

        public async Task Update(Type type)
        {
            context.Types.Update(type);
            await context.SaveChangesAsync();
        }

        public async Task Delete(Type type)
        {
            context.Types.Remove(type);
            await context.SaveChangesAsync();
        }
    }
}
