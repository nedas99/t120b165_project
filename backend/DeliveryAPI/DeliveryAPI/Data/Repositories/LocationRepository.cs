﻿using DeliveryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryAPI.Data.Repositories
{
    public interface ILocationRepository
    {
        Task<Location> Add(Location location);
        Task Delete(Location location);
        Task<Location> Get(int id);
        Task<IEnumerable<Location>> GetAll();
        Task Update(Location location);
    }

    public class LocationRepository : ILocationRepository
    {
        private readonly DataContext _context;

        public LocationRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Location>> GetAll()
        {
            var locations = await _context.Locations
                .OrderBy(l => l.Id)
                .ToListAsync();

            return locations;
        }

        public async Task<Location> Get(int id)
        {
            var location = await _context.Locations
                .FirstOrDefaultAsync(l => l.Id == id);

            return location;
        }

        public async Task<Location> Add(Location location)
        {
            _context.Locations.Add(location);
            await _context.SaveChangesAsync();
            return await Get(location.Id);
        }

        public async Task Update(Location location)
        {
            _context.Locations.Update(location);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Location location)
        {
            var actions = await _context.Actions
                .Where(a => a.LocationId == location.Id)
                .ToListAsync();

            var actionIds = actions.Select(a => a.Id);

            var items = await _context.Items
                .Where(i => actionIds.Contains((int)i.RegistrationId) || actionIds.Contains((int)i.DeliveryId))
                .Include(i => i.Registration)
                .Include(i => i.Delivery)
                .ToListAsync();

            foreach (var item in items)
            {
                if (item.RegistrationId.HasValue && actionIds.Contains((int)item.RegistrationId))
                {
                    item.Registration = null;
                }
                if (item.DeliveryId.HasValue && actionIds.Contains((int)item.DeliveryId))
                {
                    item.Delivery = null;
                }
            }

            _context.Actions.RemoveRange(actions);

            _context.Locations.Remove(location);
            await _context.SaveChangesAsync();
        }
    }
}
