﻿using DeliveryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryAPI.Data.Repositories
{
    public interface IItemRepository
    {
        Task<Item> Add(Item item);
        Task Delete(int itemId);
        Task<int> Count(int userId);
        Task<int> RegisteredCount();
        Task<int> AllCount();
        Task<Item> Get(int id);
        Task<IEnumerable<Item>> GetAll();
        Task<IEnumerable<Item>> GetAll(int userId);
        Task<IEnumerable<Item>> GetAll(int userId, int page, int pageSize);
        Task<IEnumerable<Item>> GetAll(int page, int pageSize);
        Task<IEnumerable<Item>> GetRegistered(int page, int pageSize);
        Task<Item> Update(Item item);
        Task DeleteAction(int itemId, int actionId);
        Task<Action> AddAction(int itemId, bool delivery, Action action);
    }

    public class ItemRepository : IItemRepository
    {
        private readonly DataContext context;

        public ItemRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> Count(int userId)
        {
            var count = await context.Items
                .Where(i => i.CourierId == userId || i.SenderId == userId)
                .CountAsync();

            return count;
        }

        public async Task<IEnumerable<Item>> GetAll(int page, int pageSize)
        {
            var items = await context.Items
                .OrderBy(i => i.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Include(i => i.Status)
                .Include(i => i.Type)
                .ToListAsync();

            return items;
        }

        public async Task<int> AllCount()
        {
            var count = await context.Items
                .CountAsync();

            return count;
        }

        public async Task<IEnumerable<Item>> GetAll(int userId)
        {
            var items = await context.Items
                .Where(i => i.CourierId == userId || i.SenderId == userId)
                .OrderBy(i => i.Id)
                .Include(i => i.Status)
                .Include(i => i.Type)
                .ToListAsync();

            return items;
        }

        public async Task<IEnumerable<Item>> GetAll(int userId, int page, int pageSize)
        {
            var items = await context.Items
                .Where(i => i.CourierId == userId || i.SenderId == userId)
                .OrderBy(i => i.Id)
                .Include(i => i.Status)
                .Include(i => i.Type)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return items;
        }

        public async Task<IEnumerable<Item>> GetRegistered(int page, int pageSize)
        {
            var items = await context.Items
                .Where(i => i.StatusId == 1)
                .Where(i => i.SenderId != null && i.CourierId == null)
                .OrderBy(i => i.Id)
                .Include(i => i.Status)
                .Include(i => i.Type)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return items;
        }

        public async Task<int> RegisteredCount()
        {
            var count = await context.Items
                .Where(i => i.StatusId == 1)
                .Where(i => i.SenderId != null && i.CourierId == null)
                .CountAsync();

            return count;
        }

        public async Task<IEnumerable<Item>> GetAll()
        {
            var items = await context.Items
                .OrderBy(i => i.Id)
                .Include(i => i.Status)
                .Include(i => i.Type)
                .ToListAsync();

            return items;
        }

        public async Task<Item> Get(int id)
        {
            var item = await context.Items
                .Include(i => i.Status)
                .Include(i => i.Type)
                .FirstOrDefaultAsync(i => i.Id == id);

            return item;
        }

        public async Task<Item> Add(Item item)
        {
            context.Items.Add(item);
            await context.SaveChangesAsync();
            return await Get(item.Id);
        }

        public async Task<Item> Update(Item item)
        {
            context.Items.Update(item);
            await context.SaveChangesAsync();
            return await Get(item.Id);
        }

        public async Task Delete(Item item)
        {
            context.Items.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task Delete(int itemId)
        {
            var item = await context.Items
                .Include(i => i.Registration)
                .Include(i => i.Delivery)
                .FirstOrDefaultAsync(i => i.Id == itemId);
            
            if (item != null)
            {
                if (item.Delivery != null)
                    context.Actions.Remove(item.Delivery);

                if (item.Registration != null)
                    context.Actions.Remove(item.Registration);

                context.Items.Remove(item);

                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteAction(int itemId, int actionId)
        {
            var item = await context.Items
                .Include(i => i.Registration)
                .Include(i => i.Delivery)
                .FirstOrDefaultAsync(i => i.Id == itemId);

            if (item != null)
            {
                if (item.DeliveryId == actionId)
                {
                    item.Delivery = null;
                }
                else if (item.RegistrationId == actionId)
                {
                    item.Registration = null;
                }

                await context.SaveChangesAsync();
            }
        }

        public async Task<Action> AddAction(int itemId, bool delivery, Action action)
        {
            var item = await context.Items
                .FirstOrDefaultAsync(i => i.Id == itemId);

            if (item != null)
            {
                context.Actions.Add(action);
                await context.SaveChangesAsync();

                if (delivery)
                {
                    var currAction = 
                        (await context.Items
                        .Include(i => i.Delivery)
                        .FirstOrDefaultAsync(i => i.Id == itemId))
                        .Delivery;

                    item.DeliveryId = action.Id;

                    if (currAction != null)
                        context.Actions.Remove(currAction);
                }
                else
                {
                    var currAction =
                        (await context.Items
                        .Include(i => i.Registration)
                        .FirstOrDefaultAsync(i => i.Id == itemId))
                        .Registration;

                    item.RegistrationId = action.Id;

                    if (currAction != null)
                        context.Actions.Remove(currAction);
                }

                await context.SaveChangesAsync();
            }

            return await context.Actions.Include(a => a.Location).FirstOrDefaultAsync(a => a.Id == action.Id);
        }
    }
}
