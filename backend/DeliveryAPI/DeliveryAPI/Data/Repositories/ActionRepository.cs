﻿using DeliveryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryAPI.Data.Repositories
{
    public interface IActionRepository
    {
        Task<Action> Add(Action action);
        Task Delete(Action action);
        Task<Action> Get(int id);
        Task<IEnumerable<Action>> GetAll(int itemId);
        Task<Action> Update(Action action);
        Task Delete(int actionId);
    }

    public class ActionRepository : IActionRepository
    {
        private readonly DataContext context;

        public ActionRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Action>> GetAll(int itemId)
        {
            var actions = await context.Items
                .Where(i => i.Id == itemId)
                .Include(i => i.Delivery)
                .ThenInclude(d => d.Location)
                .Include(i => i.Registration)
                .ThenInclude(r => r.Location)
                .Select(i => new List<Action> { i.Registration, i.Delivery })
                .FirstOrDefaultAsync();

            return actions;//.Where(a => a != null).ToList();
        }

        public async Task<Action> Get(int id)
        {
            var action = await context.Actions
                .Include(a => a.Location)
                .FirstOrDefaultAsync(a => a.Id == id);

            return action;
        }

        public async Task<Action> Add(Action action)
        {
            context.Add(action);
            await context.SaveChangesAsync();
            return await Get(action.Id);
        }

        public async Task<Action> Update(Action action)
        {
            context.Actions.Update(action);
            await context.SaveChangesAsync();
            return await Get(action.Id);
        }

        public async Task Delete(Action action)
        {
            context.Actions.Remove(action);
            await context.SaveChangesAsync();
        }

        public async Task Delete(int actionId)
        {
            var action = await context.Actions.FirstOrDefaultAsync(a => a.Id == actionId);
            if (action != null)
            {
                context.Actions.Remove(action);
                await context.SaveChangesAsync();
            }
        }
    }
}
