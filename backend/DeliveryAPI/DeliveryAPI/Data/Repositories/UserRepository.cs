﻿using DeliveryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryAPI.Data.Repositories
{
    public interface IUserRepository
    {
        Task<bool> GetVerified(string email);
        Task<RefreshToken> GetToken(string token);
        Task RevokeToken(RefreshToken refreshToken);
        Task<User> Add(User user);
        Task<User> Get(int id);
        Task<User> Get(string email);
        Task<User> GetByRefreshToken(string token);
        Task<IEnumerable<User>> GetAll();
        Task<IEnumerable<User>> GetAll(int page, int pageSize);
        Task Update(User user);
        Task Delete(int userId);
        Task AddRefreshToken(string email, RefreshToken token);
        Task RemoveInactiveRefreshTokens(string email);
        Task<int> Count();
    }

    public class UserRepository : IUserRepository
    {
        private readonly DataContext context;

        public UserRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<bool> GetVerified(string email)
        {
            var verified = (await context.Users
                .FirstOrDefaultAsync(u => u.Email == email)).Verified;

            return verified;
        }

        public async Task<RefreshToken> GetToken(string token)
        {
            var refreshToken = await context.RefreshTokens
                .FirstOrDefaultAsync(r => r.Token == token);

            return refreshToken;
        }

        public async Task RevokeToken(RefreshToken refreshToken)
        {
            refreshToken.Revoked = DateTime.UtcNow;

            await context.SaveChangesAsync();
        }

        public async Task<int> Count()
        {
            var count = await context.Users
                .CountAsync();

            return count;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            var users = await context.Users
                .OrderBy(u => u.Id)
                .ToListAsync();

            return users;
        }

        public async Task<IEnumerable<User>> GetAll(int page, int pageSize)
        {
            var users = await context.Users
                .OrderBy(u => u.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return users;
        }

        public async Task<User> Get(int id)
        {
            var user = await context.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            return user;
        }

        public async Task<User> Get(string email)
        {
            var user = await context.Users
                .FirstOrDefaultAsync(u => u.Email.Equals(email));

            return user;
        }

        public async Task<User> GetByRefreshToken(string token)
        {
            var user = await context.Users
                .FirstOrDefaultAsync(u => u.RefreshTokens.Any(t => t.Token == token));

            return user;
        }

        public async Task<User> Add(User user)
        {
            context.Users.Add(user);
            await context.SaveChangesAsync();
            return user;
        }

        public async Task Update(User user)
        {
            context.Users.Update(user);
            await context.SaveChangesAsync();
        }

        public async Task Delete(int userId)
        {
            var items = await context.Items
                .Where(i => i.CourierId == userId || i.SenderId == userId)
                .Include(i => i.Sender)
                .Include(i => i.Courier)
                .ToListAsync();

            var deliveryIds = await context.Items
                .Where(i => i.CourierId == userId || i.SenderId == userId)
                .Select(i => i.DeliveryId)
                .ToListAsync();

            var registrationIds = await context.Items
                .Where(i => i.CourierId == userId || i.SenderId == userId)
                .Select(i => i.RegistrationId)
                .ToListAsync();

            var user = await context.Users
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (items != null)
            {
                var registrations = await context.Actions
                    .Where(r => registrationIds.Contains(r.Id))
                    .ToListAsync();
                var deliveries = await context.Actions
                    .Where(d => deliveryIds.Contains(d.Id))
                    .ToListAsync();

                context.Actions.RemoveRange(registrations);
                context.Actions.RemoveRange(deliveries);
            }

            context.Items.RemoveRange(items);
            context.Users.Remove(user);

            await context.SaveChangesAsync();
        }

        public async Task AddRefreshToken(string email, RefreshToken token)
        {
            var user = await Get(email);

            if (user == null)
                return;

            var tokens = await context.RefreshTokens
                .Where(r => r.UserId == user.Id && r.Revoked == null)
                .ToListAsync();

            tokens.ForEach(t => t.Revoked = DateTime.UtcNow);

            token.UserId = user.Id;
            context.RefreshTokens.Add(token);
            await context.SaveChangesAsync();
        }

        public async Task RemoveInactiveRefreshTokens(string email)
        {
            var user = await Get(email);

            if (user == null)
                return;

            //user.RefreshTokens.ToList().RemoveAll(r => r.Revoked != null);

            var tokens = await context.RefreshTokens
                .Where(t => t.UserId == user.Id && t.Revoked != null)
                .ToListAsync();

            context.RefreshTokens.RemoveRange(tokens);

            await context.SaveChangesAsync();
        }
    }
}
