﻿using DeliveryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryAPI.Data.Repositories
{
    public interface IStatusRepository
    {
        Task<Status> Add(Status status);
        Task Delete(Status status);
        Task<Status> Get(int id);
        Task<IEnumerable<Status>> GetAll();
        Task Update(Status status);
    }

    public class StatusRepository : IStatusRepository
    {
        private readonly DataContext context;

        public StatusRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Status>> GetAll()
        {
            var statuses = await context.Statuses
                .Where(s => s.Retrievable)
                .OrderBy(s => s.Id)
                .ToListAsync();

            return statuses;
        }

        public async Task<Status> Get(int id)
        {
            var status = await context.Statuses
                .FirstOrDefaultAsync(s => s.Id == id);

            return status;
        }

        public async Task<Status> Add(Status status)
        {
            context.Statuses.Add(status);
            await context.SaveChangesAsync();
            return status;
        }

        public async Task Update(Status status)
        {
            context.Statuses.Update(status);
            await context.SaveChangesAsync();
        }

        public async Task Delete(Status status)
        {
            context.Statuses.Remove(status);
            await context.SaveChangesAsync();
        }
    }
}
