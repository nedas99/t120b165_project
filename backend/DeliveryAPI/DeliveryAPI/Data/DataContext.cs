﻿using Microsoft.EntityFrameworkCore;
using DeliveryAPI.Data.Entities;

namespace DeliveryAPI.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Action> Actions { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB; Initial Catalog=DeliveryApiDB");
        }*/
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>().ToTable("Location");
            modelBuilder.Entity<Status>().ToTable("Status");
            modelBuilder.Entity<Type>().ToTable("Type");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Item>().ToTable("Item");
            modelBuilder.Entity<Action>().ToTable("Action");
            modelBuilder.Entity<RefreshToken>().ToTable("RefreshToken");

            /*modelBuilder.Entity<Item>()
                .HasOne(i => i.Courier)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Item>()
                .HasOne(i => i.Sender)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);*/



            modelBuilder.Entity<User>()
                .HasMany(u => u.CourierItems)
                .WithOne(i => i.Courier)
                .HasForeignKey(i => i.CourierId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasMany(u => u.SenderItems)
                .WithOne(i => i.Sender)
                .HasForeignKey(i => i.SenderId)
                .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<Item>()
                .HasOne(i => i.Delivery)
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Item>()
                .HasOne(i => i.Registration)
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
