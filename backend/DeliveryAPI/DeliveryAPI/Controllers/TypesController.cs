﻿using AutoMapper;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeliveryAPI.Controllers
{
    [Route("api/types")]
    [ApiController]
    public class TypesController : ControllerBase
    {
        private readonly ITypeRepository typeRepository;
        private readonly IMapper mapper;

        public TypesController(ITypeRepository typeRepository, IMapper mapper)
        {
            this.typeRepository = typeRepository;
            this.mapper = mapper;
        }

        // GET: api/<TypesController>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Type>>> GetAll()
        {
            var types = await typeRepository.GetAll();
            return Ok(types);
        }

        // GET api/<TypesController>/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Type>> GetById(int id)
        {
            var type = await typeRepository.Get(id);

            if (type == null)
            {
                ModelState.AddModelError("type", $"Type with id {id} not found");
                return NotFound(ModelState);
            }

            return Ok(type);
        }

        // POST api/<TypesController>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Post(TypeDTO model)
        {
            var type = mapper.Map<Data.Entities.Type>(model);
            var typeFromDb = await typeRepository.Add(type);

            return CreatedAtAction(nameof(GetById), new { id = typeFromDb.Id }, typeFromDb);
        }

        // PUT api/<TypesController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Update(int id, TypeDTO model)
        {
            var type = await typeRepository.Get(id);

            if (type == null)
            {
                ModelState.AddModelError("type", $"Type with id {id} not found");
                return NotFound(ModelState);
            }

            mapper.Map(model, type);
            await typeRepository.Update(type);

            return Ok(type);
        }

        // DELETE api/<TypesController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            var type = await typeRepository.Get(id);

            if (type == null)
            {
                ModelState.AddModelError("type", $"Type with id {id} not found");
                return NotFound(ModelState);
            }

            await typeRepository.Delete(type);
            return NoContent();
        }
    }
}
