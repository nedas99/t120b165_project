﻿using AutoMapper;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Entities;
using DeliveryAPI.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeliveryAPI.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IUserRepository userRepository;
        private readonly IAuthorizeService authorizeService;

        public UserController(IMapper mapper, IUserRepository userRepository, IAuthorizeService authorizeService)
        {
            this.mapper = mapper;
            this.userRepository = userRepository;
            this.authorizeService = authorizeService;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserList>> GetAll(int page = 1, int pageSize = 20)
        {
            var users = await userRepository.GetAll(page, pageSize);
            var mapped = mapper.Map<IEnumerable<UserDTO>>(users);
            var count = await userRepository.Count();
            var previous = page > 1;
            var next = page * pageSize < count;
            return Ok(new UserList(previous, next, count, mapped));
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {
            var user = await userRepository.Get(id);

            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {id} not found");
                return NotFound(ModelState);
            }

            var mapped = mapper.Map<UserDTO>(user);

            return Ok(mapped);
        }

        [HttpPost]
        public async Task<ActionResult> Post(CreateUserDTO userDTO)
        {
            var userByEmail = await userRepository.Get(userDTO.Email);
            if (userByEmail != null)
            {
                ModelState.AddModelError("email", "User with this email already exists");
                return BadRequest(ModelState);
            }

            var user = mapper.Map<User>(userDTO);

            var hashed = authorizeService.GenerateHash(userDTO.Password);
            user.Password = hashed;

            var verified = user.Role != Role.Courier;
            user.Verified = verified;

            var userFromDb = await userRepository.Add(user);

            return CreatedAtAction(nameof(GetById), new { id = userFromDb.Id }, mapper.Map<UserDTO>(userFromDb));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Update(int id, UpdateUserDTO userDTO)
        {
            var user = await userRepository.Get(id);

            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {id} not found");
                return NotFound(ModelState);
            }

            mapper.Map(userDTO, user);

            await userRepository.Update(user);

            return Ok(mapper.Map<UserDTO>(user));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            var user = await userRepository.Get(id);

            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {id} not found");
                return NotFound(ModelState);
            }

            await userRepository.Delete(id);
            return NoContent();
        }
    }
}
