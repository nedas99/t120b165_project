﻿using AutoMapper;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Entities;
using DeliveryAPI.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeliveryAPI.Controllers
{
    [ApiController]
    [Route("api/users/{userId}/items/{itemId}/actions")]
    public class ActionController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IItemRepository itemRepository;
        private readonly IUserRepository userRepository;
        private readonly IActionRepository actionRepository;
        private readonly ILocationRepository locationRepository;

        public ActionController(
            IMapper mapper,
            IItemRepository itemRepository,
            IUserRepository userRepository,
            IActionRepository actionRepository,
            ILocationRepository locationRepository)
        {
            this.mapper = mapper;
            this.itemRepository = itemRepository;
            this.userRepository = userRepository;
            this.actionRepository = actionRepository;
            this.locationRepository = locationRepository;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ActionDTO>>> GetAll(int userId, int itemId)
        {
            var user = await userRepository.Get(userId);
            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;

            if (roleFromToken == Role.Sender.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            var actions = await actionRepository.GetAll(itemId);
            var mapped = mapper.Map<IEnumerable<ActionDTO>>(actions);
            return Ok(mapped);
        }

        [HttpGet("{actionId}")]
        [Authorize]
        public async Task<ActionResult<ActionDTO>> Get(int userId, int itemId, int actionId)
        {
            var user = await userRepository.Get(userId);
            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken != Role.Admin.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            if (item.RegistrationId == actionId || item.DeliveryId == actionId)
            {
                var action = await actionRepository.Get(actionId);
                return Ok(mapper.Map<ActionDTO>(action));
            }

            ModelState.AddModelError("action", $"Items action with id {actionId} not found");
            return NotFound(ModelState);
        }

        [HttpDelete("{actionId}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int userId, int itemId, int actionId)
        {
            var user = await userRepository.Get(userId);
            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            if (item.RegistrationId == actionId || item.DeliveryId == actionId)
            {
                await itemRepository.DeleteAction(itemId, actionId);
                await actionRepository.Delete(actionId);
                return NoContent();
            }

            ModelState.AddModelError("action", $"Items action with id {actionId} not found");
            return NotFound(ModelState);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Create(int userId, int itemId, CreateActionDTO actionDto)
        {
            if (actionDto.PickupLocation == null && actionDto.DeliveryLocation == null ||
                actionDto.PickupLocation != null && actionDto.DeliveryLocation != null)
            {
                ModelState.AddModelError("location", "There should be exactly one location in the request's body");
                return BadRequest(ModelState);
            }

            var user = await userRepository.Get(userId);
            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken != Role.Admin.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            var locationId = actionDto.DeliveryLocation ?? (int)actionDto.PickupLocation;

            var location = await locationRepository.Get(locationId);

            if (location == null)
            {
                ModelState.AddModelError("location", $"Location with id {locationId} not found");
                return NotFound(ModelState);
            }

            var action = mapper.Map<Action>(actionDto);
            action.LocationId = locationId;

            var delivery = actionDto.DeliveryLocation != null;

            var actionFromDb = await itemRepository.AddAction(itemId, delivery, action);

            return CreatedAtAction(nameof(Get), new { userId, itemId, actionId = actionFromDb.Id }, mapper.Map<ActionDTO>(actionFromDb));
        }

        [HttpPut("{actionId}")]
        [Authorize]
        public async Task<ActionResult> Update(int userId, int itemId, int actionId, UpdateActionDTO actionDTO)
        {
            var user = await userRepository.Get(userId);
            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken == Role.Sender.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            var location = await locationRepository.Get((int)actionDTO.Location);

            if (location == null)
            {
                ModelState.AddModelError("location", $"Location with id {actionDTO.Location} not found");
                return NotFound(ModelState);
            }

            var action = await actionRepository.Get(actionId);
            mapper.Map(actionDTO, action);
            action.LocationId = (int)actionDTO.Location;
            var actionFromDb = await actionRepository.Update(action);

            return Ok(mapper.Map<ActionDTO>(actionFromDb));
        }
    }
}
