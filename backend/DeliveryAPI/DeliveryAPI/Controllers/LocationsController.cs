﻿using AutoMapper;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Entities;
using DeliveryAPI.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeliveryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LocationsController : ControllerBase
    {
        private readonly ILocationRepository locationRepository;
        private readonly IMapper mapper;

        public LocationsController(ILocationRepository locationRepository, IMapper mapper)
        {
            this.locationRepository = locationRepository;
            this.mapper = mapper;
        }

        // GET: api/<LocationsController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Location>>> Get()
        {
            var locations = await locationRepository.GetAll();
            return Ok(locations);
        }

        // GET api/<LocationsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Location>> Get(int id)
        {
            var location = await locationRepository.Get(id);
            if (location == null)
            {
                ModelState.AddModelError("location", $"Location with id {id} not found");
                return NotFound(ModelState);
            }
            return Ok(location);
        }

        // POST api/<LocationsController>
        [HttpPost]
        public async Task<ActionResult> Post(LocationDTO dto)
        {
            var location = mapper.Map<Location>(dto);
            var locationFromDb = await locationRepository.Add(location);

            return CreatedAtAction(nameof(Get), new { id = locationFromDb.Id }, locationFromDb);
        }

        // PUT api/<LocationsController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, LocationDTO dto)
        {
            var location = await locationRepository.Get(id);

            if (location == null)
            {
                ModelState.AddModelError("location", $"Location with id {id} not found");
                return NotFound(ModelState);
            }

            mapper.Map(dto, location);
            await locationRepository.Update(location);

            return Ok(location);
        }

        // DELETE api/<LocationsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var location = await locationRepository.Get(id);

            if (location == null)
            {
                ModelState.AddModelError("location", $"Location with id {id} not found");
                return NotFound(ModelState);
            }

            await locationRepository.Delete(location);
            return NoContent();
        }
    }
}
