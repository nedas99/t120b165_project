﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using DeliveryAPI.Data;
using Microsoft.AspNetCore.Authorization;
using DeliveryAPI.Data.Repositories;

namespace DeliveryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private readonly IAuthorizeService authorizeService;
        private readonly IUserRepository userRepository;

        public AuthorizeController(IAuthorizeService authorizeService, IUserRepository userRepository)
        {
            this.authorizeService = authorizeService;
            this.userRepository = userRepository;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResponseDTO>> Login(LoginDTO loginDTO)
        {
            var verified =
                await authorizeService.VerifyPassword(loginDTO.Email, loginDTO.Password) &&
                await userRepository.GetVerified(loginDTO.Email);

            if (!verified)
            {
                return Unauthorized();
            }

            var jwtToken = await authorizeService.GenerateToken(loginDTO.Email);
            var refreshToken = authorizeService.GenerateRefreshToken();

            await userRepository.AddRefreshToken(loginDTO.Email, refreshToken);
            await userRepository.RemoveInactiveRefreshTokens(loginDTO.Email);

            SetTokenCookie(refreshToken);

            return Ok(new LoginResponseDTO(jwtToken, refreshToken.User.Role.ToString(), refreshToken.UserId));
        }

        [HttpPost("refreshToken")]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResponseDTO>> RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var oldToken = await userRepository.GetToken(refreshToken);

            Console.WriteLine(refreshToken ?? "its null");

            if (oldToken == null || oldToken.Expires <= DateTime.UtcNow || oldToken.Revoked != null)
            {
                return Unauthorized();
            }

            var user = await userRepository.GetByRefreshToken(refreshToken);
            var newToken = authorizeService.GenerateRefreshToken();

            await userRepository.AddRefreshToken(user.Email, newToken);
            await userRepository.RemoveInactiveRefreshTokens(user.Email);

            SetTokenCookie(newToken);

            var jwtToken = await authorizeService.GenerateToken(user.Email);

            return Ok(new LoginResponseDTO(jwtToken, newToken.User.Role.ToString(), newToken.UserId));
        }

        [HttpPost("revokeToken")]
        public async Task<ActionResult> RevokeToken(RevokeTokenRequest model)
        {
            var tokenValue = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(tokenValue))
            {
                return BadRequest();
            }

            var token = await userRepository.GetToken(tokenValue);
            if (token == null || token.Expires <= DateTime.UtcNow || token.Revoked != null)
            {
                return BadRequest();
            }

            await userRepository.RevokeToken(token);
            return NoContent();
        }

        private void SetTokenCookie(RefreshToken token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = token.Expires
            };
            Response.Cookies.Append("refreshToken", token.Token, cookieOptions);
        }
    }
}
