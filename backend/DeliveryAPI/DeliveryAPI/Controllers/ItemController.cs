﻿using AutoMapper;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Entities;
using DeliveryAPI.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;

namespace DeliveryAPI.Controllers
{
    [Route("api/users/{userId}/items")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IItemRepository itemRepository;
        private readonly IUserRepository userRepository;
        private readonly ITypeRepository typeRepository;
        private readonly IStatusRepository statusRepository;

        public ItemController(
            IMapper mapper, 
            IItemRepository itemRepository, 
            IUserRepository userRepository,
            ITypeRepository typeRepository,
            IStatusRepository statusRepository)
        {
            this.mapper = mapper;
            this.itemRepository = itemRepository;
            this.userRepository = userRepository;
            this.typeRepository = typeRepository;
            this.statusRepository = statusRepository;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<ItemList>> GetAll(int userId, int page = 1, int pageSize = 20)
        {
            if (page < 1)
            {
                ModelState.AddModelError("page", "Page should be a positive integer");
                return NotFound();
            }

            var user = await userRepository.Get(userId);
            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken != Role.Admin.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var items = await itemRepository.GetAll(userId, page, pageSize);

            if (items == null || !items.Any())
            {
                return NoContent();
            }

            var mapped = mapper.Map<IEnumerable<ItemDTO>>(items);

            var count = await itemRepository.Count(userId);

            var previous = page > 1;
            var next = page * pageSize < count;

            return Ok(new ItemList(previous, next, mapped.Count(), mapped));
        }

        [HttpGet("~/api/items/registered")]
        [Authorize(Roles = "Admin, Courier")]
        public async Task<ActionResult<ItemList>> GetRegistered(int page = 1, int pageSize = 20)
        {
            if (page < 1)
            {
                ModelState.AddModelError("page", "Page should be a positive integer");
                return NotFound();
            }

            var items = await itemRepository.GetRegistered(page, pageSize);

            if (items == null || !items.Any())
            {
                return NoContent();
            }

            var mapped = mapper.Map<IEnumerable<ItemDTO>>(items);

            var count = await itemRepository.RegisteredCount();

            var previous = page > 1;
            var next = page * pageSize < count;

            return Ok(new ItemList(previous, next, mapped.Count(), mapped));
        }

        [HttpGet("~/api/items")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<ItemList>> GetAll(int page = 1, int pageSize = 20)
        {
            if (page < 1)
            {
                ModelState.AddModelError("page", "Page should be a positive integer");
                return NotFound();
            }

            var items = await itemRepository.GetAll(page, pageSize);

            if (items == null || !items.Any())
            {
                return NoContent();
            }

            var mapped = mapper.Map<IEnumerable<ItemDTO>>(items);

            var count = await itemRepository.RegisteredCount();

            var previous = page > 1;
            var next = page * pageSize < count;

            return Ok(new ItemList(previous, next, mapped.Count(), mapped));
        }

        [HttpGet("{itemId}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ItemDTO>>> GetById(int userId, int itemId)
        {
            var user = await userRepository.Get(userId);

            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken != Role.Admin.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            var mapped = mapper.Map<ItemDTO>(item);

            return Ok(mapped);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Post(int userId, [FromBody]CreateItemDTO itemDTO)
        {
            var typeFromDb = await typeRepository.Get((int)itemDTO.Type);
            var userFromDb = await userRepository.Get(userId);

            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (typeFromDb == null)
                errors.Add("type", $"Type with id {itemDTO.Type} not found");
            if (userFromDb == null)
                errors.Add("user", $"User with id {userId} not found");

            if (errors.Count > 0)
            {
                foreach (var key in errors.Keys)
                    ModelState.AddModelError(key, errors[key]);
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken != Role.Sender.ToString() && idFromToken != userFromDb.Id.ToString())
            {
                return Forbid();
            }

            var item = mapper.Map<Item>(itemDTO);
            item.SenderId = userId;
            item.StatusId = 1;
            var itemFromDb = await itemRepository.Add(item);

            return CreatedAtAction(nameof(GetById), new {itemId = itemFromDb.Id, userId}, mapper.Map<ItemDTO>(itemFromDb));
        }

        [HttpPut("{itemId}")]
        public async Task<ActionResult> Put(int userId, int itemId, UpdateItemDTO itemDTO)
        {
            var typeFromDb = await typeRepository.Get((int)itemDTO.Type);
            var statusFromDb = await statusRepository.Get((int)itemDTO.Status);
            var userFromDb = await userRepository.Get(userId);
            var itemFromDb = await itemRepository.Get(itemId);

            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (typeFromDb == null)
                errors.Add("type", $"Type with id {itemDTO.Type} not found");
            if (statusFromDb == null)
                errors.Add("status", $"Status with id {itemDTO.Status} not found");
            if (userFromDb == null)
                errors.Add("user", $"User with id {userId} not found");

            if (itemFromDb == null)
                errors.Add("item", $"Item with id {itemId} not found");
            else if (itemFromDb.SenderId != userId && itemFromDb.CourierId != userId)
                errors.Add("item", $"Users item with id {itemId} not found");

            if (errors.Count > 0)
            {
                foreach (var key in errors.Keys)
                    ModelState.AddModelError(key, errors[key]);
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken == Role.Sender.ToString() && idFromToken != userFromDb.Id.ToString())
            {
                return Forbid();
            }

            mapper.Map(itemDTO, itemFromDb);
            var updatedItem = await itemRepository.Update(itemFromDb);

            return Ok(mapper.Map<ItemDTO>(updatedItem));
        }

        [HttpDelete("{itemId}")]
        public async Task<ActionResult> Delete(int userId, int itemId)
        {
            var user = await userRepository.Get(userId);

            if (user == null)
            {
                ModelState.AddModelError("user", $"User with id {userId} not found");
                return NotFound(ModelState);
            }

            var idFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var roleFromToken = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (roleFromToken == Role.Courier.ToString() || 
                roleFromToken != Role.Admin.ToString() && idFromToken != user.Id.ToString())
            {
                return Forbid();
            }

            var item = await itemRepository.Get(itemId);
            if (item == null || item.SenderId != userId && item.CourierId != userId)
            {
                ModelState.AddModelError("item", $"Users item with id {itemId} not found");
                return NotFound(ModelState);
            }

            await itemRepository.Delete(itemId);
            return NoContent();
        }
    }
}
