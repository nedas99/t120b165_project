﻿using AutoMapper;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Entities;
using DeliveryAPI.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeliveryAPI.Controllers
{
    [ApiController]
    [Route("api/statuses")]
    public class StatusController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IStatusRepository statusRepository;

        public StatusController(IMapper mapper, IStatusRepository statusRepository)
        {
            this.mapper = mapper;
            this.statusRepository = statusRepository;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Status>>> GetAll()
        {
            var statuses = await statusRepository.GetAll();
            return Ok(statuses);
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Status>> GetById(int id)
        {
            var status = await statusRepository.Get(id);

            if (status == null)
            {
                ModelState.AddModelError("status", $"Status with id {id} not found");
                return NotFound(ModelState);
            }

            return Ok(status);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            var status = await statusRepository.Get(id);

            if (status == null)
            {
                ModelState.AddModelError("status", $"Status with id {id} not found");
                return NotFound(ModelState);
            }

            await statusRepository.Delete(status);
            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Post(StatusDTO statusDTO)
        {
            var status = mapper.Map<Status>(statusDTO);
            status.Retrievable = true;
            var statusFromDb = await statusRepository.Add(status);

            return CreatedAtAction(nameof(GetById), new { id = status.Id }, status);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Update(int id, StatusDTO statusDTO)
        {
            var status = await statusRepository.Get(id);

            if (status == null)
            {
                ModelState.AddModelError("status", $"Status with id {id} not found");
                return NotFound(ModelState);
            }

            mapper.Map(statusDTO, status);
            await statusRepository.Update(status);

            return Ok(status);
        }
    }
}
