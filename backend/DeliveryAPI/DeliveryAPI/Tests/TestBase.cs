﻿using System;
using DeliveryAPI.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace DeliveryAPI.Tests
{
    public class TestBase : IDisposable
    {
        private const string InMemoryConnectionString = "DataSource=:memory:";
        private SqliteConnection conn;

        protected DataContext DbContext;

        protected TestBase()
        {
            conn = new SqliteConnection(InMemoryConnectionString);
            conn.Open();

            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlite(conn)
                .Options;

            DbContext = new DataContext(options);
            DbContext.Database.EnsureCreated();
        }

        public void ClearDb()
        {
            conn = new SqliteConnection(InMemoryConnectionString);
            conn.Open();

            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlite(conn)
                .Options;

            DbContext = new DataContext(options);
            DbContext.Database.EnsureCreated();
        }

        public void Dispose()
        {
            conn.Close();
        }
    }
}
