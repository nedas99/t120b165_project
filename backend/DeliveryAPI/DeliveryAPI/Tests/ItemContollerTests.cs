﻿using AutoMapper;
using DeliveryAPI.Controllers;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Repositories;
using DeliveryAPI.Data.Entities;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Moq;

namespace DeliveryAPI.Tests
{
    public class ItemContollerTests : TestBase
    {
        private readonly ItemController controller;
        private readonly IItemRepository itemRepository;
        private readonly IUserRepository userRepository;
        private readonly ITypeRepository typeRepository;
        private readonly IStatusRepository statusRepository;

        public ItemContollerTests()
        {
            var mockMapper = new MapperConfiguration(cfg => cfg.AddProfile(new Profiles()));
            itemRepository = new ItemRepository(DbContext);
            userRepository = new UserRepository(DbContext);
            typeRepository = new TypeRepository(DbContext);
            statusRepository = new StatusRepository(DbContext);
            var mockConfig = new JwtConfig { SecretKey = "a" };
            var auth = new AuthorizeService(userRepository, mockConfig);

            controller = 
                new ItemController(mockMapper.CreateMapper(), 
                                   itemRepository, 
                                   userRepository, 
                                   typeRepository, 
                                   statusRepository);
        }

        [Theory]
        [InlineData(5, 1, 5)]
        [InlineData(30, 2, 10)]
        [InlineData(50, 1, 20)]
        [InlineData(10, 2, 0)]
        public async Task GetAll(int totalCount, int page, int count)
        {
            // Arrange

            ClearDb();
            User user = new User
            {
                Email = "sender@gmail.com",
                Password = "aaa",
                PhoneNumber = "123456",
                Role = Role.Sender
            };

            user = await userRepository.Add(user);
            await typeRepository.Add(new Type { Name = "type1" });
            await statusRepository.Add(new Status { Name = "status1" });

            for (int i = 1; i <= totalCount; i++)
            {
                await itemRepository.Add(new Item
                {
                    SenderId = user.Id,
                    Height = 1,
                    Length = 1,
                    Width = 1,
                    TypeId = 1,
                    StatusId = 1,
                    Name = $"item{i}"
                });
            }

            controller.ControllerContext = new ControllerContext();
            var httpContextMock = new Mock<HttpContext>();

            httpContextMock
                .Setup(c => c.User)
                .Returns(new ClaimsPrincipal(
                    new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                        new Claim(ClaimTypes.Role, user.Role.ToString())
                    })));

            controller.ControllerContext.HttpContext = httpContextMock.Object;

            // Act

            var response = await controller.GetAll(user.Id, page);

            // Assert

            if (count == 0)
            {
                Assert.IsType<NoContentResult>(response.Result);
            }
            else
            {
                var result = Assert.IsType<OkObjectResult>(response.Result);
                var obj = Assert.IsType<ItemList>(result.Value);
                Assert.Equal(count, obj.Count);
            }
        }
    }
}
