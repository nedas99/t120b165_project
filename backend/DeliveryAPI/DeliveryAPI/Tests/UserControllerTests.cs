﻿using AutoMapper;
using DeliveryAPI.Controllers;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Repositories;
using DeliveryAPI.Data.Entities;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace DeliveryAPI.Tests
{
    public class UserControllerTests : TestBase
    {
        private readonly UserController controller;
        private readonly IUserRepository userRepository;

        public UserControllerTests()
        {
            var mockMapper = new MapperConfiguration(cfg => cfg.AddProfile(new Profiles()));
            userRepository = new UserRepository(DbContext);
            var mockConfig = new JwtConfig { SecretKey = "a" };
            var auth = new AuthorizeService(userRepository, mockConfig);
            controller = new UserController(mockMapper.CreateMapper(), userRepository, auth);
        }

        [Fact]
        public async Task AddUser_ReturnsOkAndNewlyCreatedUser()
        {
            // Arrange

            ClearDb();
            CreateUserDTO body = new CreateUserDTO("email@gmail.com", "123456789", Role.Admin, "pass1");

            // Act

            var response = await controller.Post(body);

            // Assert

            var result = Assert.IsType<CreatedAtActionResult>(response);
            var user = Assert.IsType<UserDTO>(result.Value);
            Assert.Equal(body.Email, user.Email);
            Assert.Equal(body.PhoneNumber, user.PhoneNumber);
            Assert.Equal(body.Role.ToString(), user.Role);

            Assert.Equal(1, user.Id);
        }

        [Fact]
        public async Task AddUser_ReturnsBadRequest()
        {
            // Arrange

            ClearDb();
            CreateUserDTO body = new CreateUserDTO("email@gmail.com", "123456789", Role.Admin, "pass1");
            await userRepository.Add(new User
            {
                Email = body.Email,
                PhoneNumber = "123456",
                Password = "aaa",
                Role = Role.Courier
            });

            // Act

            var response = await controller.Post(body);

            // Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task UpdateUser_ReturnsOkAndUpdatedUser()
        {
            // Arrange

            ClearDb();
            var userToDb = new User
            {
                Email = "email1@gmail.com",
                PhoneNumber = "123456789",
                Role = Role.Admin
            };
            await userRepository.Add(userToDb);
            var dto = new UpdateUserDTO("updated@gmail.com", "updated123", true);

            // Act

            var response = await controller.Update(userToDb.Id, dto);

            // Assert
            var result = Assert.IsType<OkObjectResult>(response);
            var userDto = Assert.IsType<UserDTO>(result.Value);
            Assert.Equal(dto.Email, userDto.Email);
            Assert.Equal(dto.PhoneNumber, userDto.PhoneNumber);

            Assert.Equal(userToDb.Id, userDto.Id);
        }

        [Fact]
        public async Task UpdateUser_ReturnsNotFound()
        {
            // Arrange

            ClearDb();
            var userToDb = new User
            {
                Email = "email1@gmail.com",
                PhoneNumber = "123456789",
                Role = Role.Admin
            };
            await userRepository.Add(userToDb);
            var dto = new UpdateUserDTO("updated@gmail.com", "updated123", true);

            // Act

            var response = await controller.Update(userToDb.Id + 100, dto);

            // Assert
            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public async Task DeleteUser_ReturnsNoContentAndUserDoesntExistInDatabase()
        {
            // Arrange

            ClearDb();
            var userToDb1 = new User
            {
                Email = "email1@gmail.com",
                PhoneNumber = "123456789",
                Role = Role.Admin
            };
            var userToDb2 = new User
            {
                Email = "email2@gmail.com",
                PhoneNumber = "123456789",
                Role = Role.Admin
            };
            await userRepository.Add(userToDb1);
            await userRepository.Add(userToDb2);

            // Act

            var response = await controller.Delete(userToDb1.Id);

            // Assert

            var userFromDb = await userRepository.Get(userToDb1.Id);

            Assert.IsType<NoContentResult>(response);
            Assert.Null(userFromDb);
        }

        [Fact]
        public async Task DeleteUser_ReturnsNotFound()
        {
            // Arrange

            ClearDb();
            var userToDb1 = new User
            {
                Email = "email1@gmail.com",
                PhoneNumber = "123456789",
                Role = Role.Admin
            };
            await userRepository.Add(userToDb1);

            // Act

            var response = await controller.Delete(userToDb1.Id + 100);

            // Assert

            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public async Task GetAllUsers_ReturnsOkAndUsers()
        {
            // Arrange

            ClearDb();
            List<User> users = new List<User> 
            {
                new User
                {
                    Email = "email1@gmail.com",
                    PhoneNumber = "123456789",
                    Role = Role.Admin
                },
                new User
                {
                    Email = "email2@gmail.com",
                    PhoneNumber = "987654321",
                    Role = Role.Courier
                },
                new User
                {
                    Email = "email3@gmail.com",
                    PhoneNumber = "134679258",
                    Role = Role.Sender
                }
            };
            foreach (var user in users)
            {
                await userRepository.Add(user);
            }

            // Act

            var response = await controller.GetAll();

            // Assert

            var result = Assert.IsType<OkObjectResult>(response.Result);
            var userList = Assert.IsType<UserList>(result.Value);

            Assert.Equal(users.Count, userList.Count);
            foreach (var user in users)
            {
                var userFromController = userList.Users.FirstOrDefault(u => u.Id == user.Id);
                Assert.NotNull(userFromController);
                Assert.Equal(user.Email, userFromController.Email);
                Assert.Equal(user.PhoneNumber, userFromController.PhoneNumber);
                Assert.Equal(user.Role.ToString(), userFromController.Role);
                Assert.Equal(user.Id, userFromController.Id);
            }
        }

        [Fact]
        public async Task GetUserById_ReturnsOkAndUser()
        {
            // Arrange

            ClearDb();
            List<User> users = new List<User>
            {
                new User
                {
                    Email = "email1@gmail.com",
                    PhoneNumber = "123456789",
                    Role = Role.Admin
                },
                new User
                {
                    Email = "email2@gmail.com",
                    PhoneNumber = "987654321",
                    Role = Role.Courier
                },
                new User
                {
                    Email = "email3@gmail.com",
                    PhoneNumber = "134679258",
                    Role = Role.Sender
                }
            };
            foreach (var _user in users)
            {
                await userRepository.Add(_user);
            }

            var user = users[0];

            // Act

            var response = await controller.GetById(user.Id);

            // Assert

            var result = Assert.IsType<OkObjectResult>(response.Result);
            var userFromController = Assert.IsType<UserDTO>(result.Value);

            Assert.NotNull(userFromController);
            Assert.Equal(user.Email, userFromController.Email);
            Assert.Equal(user.PhoneNumber, userFromController.PhoneNumber);
            Assert.Equal(user.Role.ToString(), userFromController.Role);
            Assert.Equal(user.Id, userFromController.Id);
        }

        [Fact]
        public async Task GetUserById_ReturnsNotFound()
        {
            // Arrange

            ClearDb();
            List<User> users = new List<User>
            {
                new User
                {
                    Email = "email1@gmail.com",
                    PhoneNumber = "123456789",
                    Role = Role.Admin
                },
                new User
                {
                    Email = "email2@gmail.com",
                    PhoneNumber = "987654321",
                    Role = Role.Courier
                },
                new User
                {
                    Email = "email3@gmail.com",
                    PhoneNumber = "134679258",
                    Role = Role.Sender
                }
            };
            foreach (var _user in users)
            {
                await userRepository.Add(_user);
            }

            var user = users[^1];

            // Act

            var response = await controller.GetById(user.Id + 100);

            // Assert

            Assert.IsType<NotFoundObjectResult>(response.Result);
        }
    }
}
