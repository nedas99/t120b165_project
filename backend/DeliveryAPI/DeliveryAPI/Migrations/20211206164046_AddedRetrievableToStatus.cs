﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryAPI.Migrations
{
    public partial class AddedRetrievableToStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Retrievable",
                table: "Status",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Retrievable",
                table: "Status");
        }
    }
}
