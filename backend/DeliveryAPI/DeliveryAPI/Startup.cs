using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using DeliveryAPI.Data;
using DeliveryAPI.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;

namespace DeliveryAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        private Microsoft.AspNetCore.Hosting.IHostingEnvironment CurrentEnvironment { get; set; }

        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment appEnv)
        {
            CurrentEnvironment = appEnv;

            Configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            string conn;
            string secretKey;

            // Hardcoded connection string and secret key only for development environment:
            if (CurrentEnvironment.IsDevelopment())
            {
                conn = "Data Source=(localdb)\\MSSQLLocalDB; Initial Catalog=DeliveryApiDB";
                secretKey = "4d2wIlO9UjZxftLozF1hnpYcYkJ33EG5sj22A5BuWedi";
            }
            else
            {
                conn = Configuration.GetConnectionString("DBConn");
                secretKey = Configuration.GetConnectionString("SecretKey");
            }

            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<DataContext>(options =>
                {
                    options.UseSqlServer(conn);
                });

            var config = new JwtConfig
            {
                SecretKey = secretKey
            };

            services.AddSingleton(config);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => 
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(config.SecretKey)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddCors(options =>
                options.AddPolicy("CorsPolicy",
                    builder => {
                        builder
                            //.WithOrigins("http://localhost:3000/")
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            //.AllowAnyOrigin()
                            .AllowCredentials()
                            .SetIsOriginAllowed(host => true);
                    }));

            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));

            services.AddTransient<IStatusRepository, StatusRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ITypeRepository, TypeRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();
            services.AddTransient<IActionRepository, ActionRepository>();
            services.AddTransient<ILocationRepository, LocationRepository>();

            services.AddTransient<IAuthorizeService, AuthorizeService>();

            services.AddTransient<DataContextSeed, DataContextSeed>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
